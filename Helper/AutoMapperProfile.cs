﻿using AutoMapper;
using rupoi.Data;
using rupoi.Data.Dictionaries;
using rupoi.Models.Dictionaries;
using rupoi.Models.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Helper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Cart, RegistrationAdd>();
            CreateMap<RegistrationAdd, Cart>();

            CreateMap<DictionaryModel, DiagnosisType>();
            CreateMap<DiagnosisType, DictionaryModel>();

            CreateMap<DictionaryModel, ScarTypes>();
            CreateMap<ScarTypes, DictionaryModel>();
        }
    }
}
