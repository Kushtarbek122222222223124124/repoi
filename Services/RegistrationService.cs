﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Models.Registration;
using rupoi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Services
{
    public class RegistrationService : ServiceBase , IRegistrationService
    {
        public RegistrationService(DbContextOptions<RupoiContext> options, IMapper mapper) : base(options, mapper)
        {
        }

        public IEnumerable<RegistrationListItem> List(RegistrationFilter filter, int start, int take)
        {
            var ret = new List<RegistrationListItem>();


            return ret;
        }

        public void SaveCart(RegistrationAdd model)
        {
            var m = mapper.Map<RegistrationAdd, Cart>(model);

            if(m.Id> 0)
            {
                contex.Update(m);
            }
            else
            {
                contex.Add(m);
            }

            contex.SaveChanges();
        }
    }
}
