﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Services
{
    public class ServiceBase
    {
        protected RupoiContext contex { get; set; }
        protected readonly IMapper mapper;

        public ServiceBase(DbContextOptions<RupoiContext> options, IMapper mapper)
        {
            this.contex = new RupoiContext(options);
            this.mapper = mapper;
        }

    }
}
