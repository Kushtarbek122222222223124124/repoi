﻿using rupoi.Models.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Services.Interfaces
{
    public interface IDictionariesService
    {
        List<DictionaryModel> GetList(DictionariesFileterModel filetr, string type);
        DictionaryModel Find(int id, string type);
        void Save(DictionaryModel model, string type);
    }
}
