﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Dictionaries
{
    public class DictionaryPageModel
    {
        public string Title { get; set; }
        public string Code { get; set; }

        public List<DictionaryModel> List{get; set;}
    }
}
