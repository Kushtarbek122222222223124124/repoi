﻿using rupoi.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Dictionaries
{
    public class DictionaryModel
    {

        public int Id { get; set; }

        [LocalizedDisplayName("DictionaryModel_Name")]
        public string Name { get; set; }
    }
}
