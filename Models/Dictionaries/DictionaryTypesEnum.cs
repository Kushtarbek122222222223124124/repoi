﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Dictionaries
{
    public enum DictionaryTypesEnum
    {
        DiagnosisType,
        ProductType,
        ScarType
    }
}
