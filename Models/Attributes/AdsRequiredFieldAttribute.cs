﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class AdsRequiredFieldAttribute : System.Attribute
    {
        public bool Required { get; private set; }

        public AdsRequiredFieldAttribute(bool isRequired = true)
        {
            Required = isRequired;
        }
    }
}
