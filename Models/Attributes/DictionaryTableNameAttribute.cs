﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class DictionaryTableNameAttribute : System.Attribute
    {
        /// <summary>
        /// Получает или задаёт наименование таблицы БД для соответствующего справочника.
        /// </summary>
        /// <value>
        /// Наименование таблицы БД для соответствующего справочника.
        /// </value>
        public string TableName { get; private set; }

        /// <summary>
        /// Получает или задаёт код таблицы БД для соответствующего справочника.
        /// </summary>
        /// <value>
        /// Код таблицы БД для соответствующего справочника.
        /// </value>
        public string TableCode { get; private set; }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="DictionaryTableNameAttribute"/>.
        /// </summary>
        /// <param name="tableName">Наименование таблицы БД для соответствующего справочника.</param>
        /// <param name="tableCode">Код таблицы БД для соответствующего справочника.</param>
        public DictionaryTableNameAttribute(string tableName, string tableCode = "")
        {
            TableName = tableName;
            TableCode = tableCode;
        }
    }
}
