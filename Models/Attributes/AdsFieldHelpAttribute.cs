﻿using rupoi.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace rupoi.Models.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class AdsFieldHelpAttribute : Attribute
    {
        public string Help { get { return GetMessageFromResource(this.resourceId, this.defaultValue); } }

        private readonly string resourceId;
        private readonly string defaultValue;

        public AdsFieldHelpAttribute(string resourceId)
        {
            this.resourceId = resourceId;
        }

        public AdsFieldHelpAttribute(string resourceId, string defaultVlaue)
        {
            this.resourceId = resourceId;
            this.defaultValue = defaultVlaue;
        }

        private static string GetMessageFromResource(string resourceName, string defaultValue)
        {
            Type type = typeof(LocResource);
            PropertyInfo nameProperty = type.GetProperty(resourceName, BindingFlags.Static | BindingFlags.Public);

            if (nameProperty != null)
            {
                object result = nameProperty.GetValue(nameProperty.DeclaringType, null);

                return result.ToString();
            }

            if (defaultValue != null)
            {
                return defaultValue;
            }

            return resourceName;
        }
    }
}
