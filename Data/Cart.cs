﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Data
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }

        [StringLength(16)]
        public string Inn { get; set; }
        public int DocumentType { get; set; }
        public string FirstName { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public int Sex { get; set; }
        public DateTime BirthDate { get; set; }

        [StringLength(5)]
        public string DocumentSeries { get; set; }

        [StringLength(10)]
        public string DocumentNumber { get; set; }

        public DateTime DocumentIssueDate { get; set; }

        [StringLength(100)]
        public string DocumentIssuedBy { get; set; }

        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(250)]
        public string PhoneNumber { get; set; }

        [StringLength(250)]
        public string LovzType { get; set; }

        public int LovzGroupe { get; set; }

        [StringLength(250)]
        public string Note { get; set; }

    }
}
