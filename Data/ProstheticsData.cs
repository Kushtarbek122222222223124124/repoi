﻿using rupoi.Data.Dictionaries;
using rupoi.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
namespace rupoi.Data
{
    public class ProstheticsData
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("CartId")]
        public Cart Cart { get; set; }

        public int Length { get; set; }

        public StumpForm StumpForm { get; set; }
        public StumpMobility StumpMobility { get; set; }
        public string StumpMobilityDescription { get; set; }
        public ScarTypes ScarType { get; set; }
        public SkinConditionTypes SkinConditionType { get; set; }
        public bool StumpSupport { get; set; }

        [StringLength(500)]
        public string ObjectivData { get; set; }

        /*[ForeignKey("ProductTypeId")]
        public ProductType ProductType { get; set; }

        [ForeignKey("DiagnosisTypeId")]
        public DiagnosisType DiagnosisType { get; set; }
        public DiagnosisSide DiagnosisSide { get; set; }
        public bool Hospitalized { get; set; }*/


    }
}
