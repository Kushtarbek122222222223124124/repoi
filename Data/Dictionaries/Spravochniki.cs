﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models
{


    // Справочник Область
    public class Region
    {
        public int Id { get; set; }

        [Display(Name = "Регион (кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Регион (рус.)")]
        public string NameRus { get; set; }

    }
    // Справочник Район
    public class DictDistrict
    {

        public int Id { get; set; }

        [Display(Name = "Район (кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Район (рус.)")]
        public string NameRus { get; set; }

        [Display(Name = "Регион (рус.)")]
        public int RegionId { get; set; }
        [Display(Name = "Регион (рус.)")]
        public Region Region { get; set; }
    }
    //Справочник  Услуга (Платная/Бесплатная)
    public class Usluga 
    {
        public int Id { get; set; }

        [Display(Name = "Услуга   (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Услуга   (наименование рус.)")]
        public string NameRus { get; set; }

      
    }

    //Справочник Документ (Список: Паспорт, Свидетельство о рождении) *
    public class Document 
    {
        public int Id { get; set; }

        [Display(Name = "Документ   (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Документ   (наименование рус.)")]
        public string NameRus { get; set; }
    }

    //Справочник Серия паспорта
    public class Seria 
    {
        public int Id { get; set; }

        [Display(Name = "Серия паспорта   (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Серия паспорта   (наименование рус.)")]
        public string NameRus { get; set; }
    }


    //Справочник	Статус заказа: Срочный/Обычный 
    public class StatusZ 
    {
        public int Id { get; set; }

        [Display(Name = "Статус заказа   (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Статус заказа")]
        public string NameRus { get; set; }
    }

    public class TipUslugi 
    {
        public int Id { get; set; }

        [Display(Name = "Тип Услуги    (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Тип Услуги    (наименование рус.)")]
        public string NameRus { get; set; }


    }

    //Справочник Диагноз 
    public class Diagnoz
    {
        public int Id { get; set; }

        [Display(Name = "Диагноз  (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Диагноз ")]
        public string NameRus { get; set; }

        }

    //Справочник Диагноз(данные)
    public class AmpN
    {
        public int Id { get; set; }


        [Display(Name = "Диагноз")]
        public int DiagnozId { get; set; }
        public Diagnoz Diagnoz { get; set; }


        [Display(Name = " Диагноз(данные)(наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Диагноз(данные)")]
        public string NameRus { get; set; }

      
    }



    //Вид изделия (Заказ на изготовление протеза)
    public class Protezn
    {

        public int Id { get; set; }


        [Display(Name = "Шифр изделия")]
        public string Shifr { get; set; }


        [Display(Name = " Вид изделия (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Вид изделия")]
        public string NameRus { get; set; }

    }


    //Модель  (Заказ на изготовление обуви)
    public class Obyv
    {

        public int Id { get; set; }

        [Display(Name = "Шифр изделия")]
        public string Shifr { get; set; }


        [Display(Name = " Модель  (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Модель ")]
        public string NameRus { get; set; }

    }

    //Вид изделия (Заказ на изготовление Оттобок)
    public class Otto
    {
        public int Id { get; set; }


        [Display(Name = "Оттобок  (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Оттобок ")]
        public string NameRus { get; set; }

    }



    //при ампутации
    public class KostOpil
    {
        public int Id { get; set; }

        [Display(Name = "Костный опил (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Костный опил (наименование рус.) ")]
        public string NameRus { get; set; }

    }

    //Описание изделия
    public class OpisIzdelia 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Описание изделия (наименование рус.) ")]
        public string NameRus { get; set; }

    }

    public class ShinoKoj 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisIzdelia OpisIzdelia { get; set; }


        [Display(Name = "Шино-кожный  (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Шино-кожный  (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class Derevyin 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisIzdelia OpisIzdelia { get; set; }


        [Display(Name = "Деревянный   (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Деревянный   (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class Golen 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisIzdelia OpisIzdelia { get; set; }


        [Display(Name = "Голень   (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Голень   (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class FiksApp 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisIzdelia OpisIzdelia { get; set; }


        [Display(Name = "Фиксационный аппарат   (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Фиксационный аппарат   (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class ProtezGoleni 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisIzdelia OpisIzdelia { get; set; }


        [Display(Name = "Протез голени по Пирогову   (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Протез голени по Пирогову   (наименование рус.)")]
        public string NameRus { get; set; }

    }

    //при ампутации
    public class PodvijnostKulti 
    {
        public int Id { get; set; }

        [Display(Name = "Подвижность культи (наименование кырг.)  ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Подвижность культи (наименование рус.) ")]
        public string NameRus { get; set; }

    }
    public class Rubez 
    {
        public int Id { get; set; }

        [Display(Name = "Рубец  (наименование кырг.) ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Рубец (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class SostKojPokr 
    {
        public int Id { get; set; }

        [Display(Name = "Состояние кожного покрова и мягких тканей культи (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Состояние кожного покрова и мягких тканей культи (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class FormaKulti 
    {
        public int Id { get; set; }

        [Display(Name = "Форма культи  (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Форма культи (наименование рус.)")]
        public string NameRus { get; set; }

    }

    // при распечатке Если ортобувь
    public class VidKrep 
    {
        public int Id { get; set; }

        [Display(Name = "Вид крепления (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Вид крепления (наименование рус.)")]
        public string NameRus { get; set; }

    }

    public class VidPadoshvy 
    {
        public int Id { get; set; }

        [Display(Name = "Вид подошвы (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Вид подошвы (наименование рус.)")]
        public string NameRus { get; set; }

    }

    // описание изделия
    public class OpisanieIzdelia 
    {
        public int Id { get; set; }

        [Display(Name = "Описание изделия (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Описание изделия (наименование рус.)")]
        public string NameRus { get; set; }

        public List<KJKsOb> KJKsOb { get; set; }

    }
    public class KJKsOb 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisanieIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisanieIzdelia OpisanieIzdelia { get; set; }


        [Display(Name = "Круговой жесткий корсет с обеих сторон до носка  (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Круговой жесткий корсет с обеих сторон до носка  (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class KJKsVnut 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisanieIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisanieIzdelia OpisanieIzdelia { get; set; }


        [Display(Name = "Круговой жесткий корсет с внутренней стороны до носка  (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Круговой жесткий корсет с внутренней стороны до носка  (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class MetalPlastik 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisanieIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisanieIzdelia OpisanieIzdelia { get; set; }


        [Display(Name = "Металлическая пластинка подошвы  (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Металлическая пластинка подошвы  (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class UvelichRazmery 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisanieIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisanieIzdelia OpisanieIzdelia { get; set; }


        [Display(Name = "Увеличенные размеры  (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Увеличенные размеры  (наименование рус.)")]
        public string NameRus { get; set; }

    }
    public class UkorochenieNK 
    {
        public int Id { get; set; }


        [Display(Name = "Описание изделия ")]
        public int OpisanieIzdeliaId { get; set; }

        [Display(Name = "Описание изделия ")]
        public OpisanieIzdelia OpisanieIzdelia { get; set; }


        [Display(Name = "Укорочение нижних конечностей  (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Укорочение нижних конечностей  (наименование рус.)")]
        public string NameRus { get; set; }

    }
    
    // при распечатке Если ортобувь++

    public class SvetIzdelia 
    {
        public int Id { get; set; }

        [Display(Name = "Цвет изделия (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Цвет изделия (наименование рус.)")]
        public string NameRus { get; set; }

    }

    // при распечатке Если протез
    public class NamePFabrikat  
    {
        public int Id { get; set; }

        [Display(Name = "Наименование полуфабрикатов  ( кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Наименование полуфабрикатов ( рус.)")]
        public string NameRus { get; set; }

        //public List<ShifrPFabrikat> ShifrPFabrikat { get; set; }
    }

    public class ShifrPFabrikat
    {
        public int Id { get; set; }

        [Display(Name = "Наименование полуфабрикатов")]
        public int NamePFabrikatId { get; set; }

        [Display(Name = "Наименование полуфабрикатов")]
        public NamePFabrikat NamePFabrikat { get; set; }


        [Display(Name = "Шифр полуфабрикатов ( кырг.) ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Шифр полуфабрикатов ( рус.)")]
        public string NameRus { get; set; }

    }
    //Категория инвалидности
    public class CategoriaInv 
    {
        public int Id { get; set; }

        [Display(Name = "Категория инвалидности (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Категория инвалидности (наименование рус.)")]
        public string NameRus { get; set; }

    }
    //Причина  инвалидности
    public class PrichinaInv 
    {
        public int Id { get; set; }

        [Display(Name = "Причина инвалидности (наименование кырг.)")]
        public string NameKyrg { get; set; }

        [Display(Name = "Причина инвалидности (наименование рус.)")]
        public string NameRus { get; set; }

    }

    public class AgeGroup
    {
        public int Id { get; set; }

        [Display(Name = "Возрастная группа ")]
        public string NameKyrg { get; set; }

        [Display(Name = "Возрастная группа ")]
        public string NameRus { get; set; }

    }


}
