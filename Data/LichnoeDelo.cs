﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using rupoi.Models;
using rupoi.Data;

namespace rupoi.Models
{
   // Таблица Личное дело
    public class LichnoeDelo
    {
        public int Id { get; set; }


        [Display(Name = "№ личного дела")]
        public string Number_LD { get; set; }


        [Display(Name = "Дата создания")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? CreateDate { get; set; }


        [Display(Name = "ПИН ")]
        [StringLength(maximumLength: 14, MinimumLength = 14, ErrorMessage = "ПИН должен быть 14 знаков")]
        [Required(ErrorMessage = "Обязательное поле")]
        [Index(IsUnique = true)]
        public string PIN { get; set; }


        [Display(Name = "Документ")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Document { get; set; }


        [Display(Name = "	Фамилия  ")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string SurName { get; set; }


        [Display(Name = "	Имя  ")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Name { get; set; }


        [Display(Name = "	Отчество ")]
        public string Middle_Name { get; set; }


        [Display(Name = "Пол")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Pol { get; set; }


        [Display(Name = "Год рождения ")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? BirthDate { get; set; }


        [Display(Name = "	Серия ")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Series { get; set; }


        [Display(Name = "№ Паспорта")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Passport_Num { get; set; }


        [Display(Name = "	Орган выдачи ")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Organ_P_Vyd { get; set; }


        [Display(Name = "	Дата выдачи ")]
        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? Data_Vyd { get; set; }


        [Display(Name = "№ Пенсионного удостоверения ")]
        public string Pension_Num { get; set; }


        [Display(Name = "Орган выдачи ")]
        public string Organ_U_Vyd { get; set; }


        [Display(Name = "	Дата выдачи ")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? Data_U_Vyd { get; set; }


        [Display(Name = "Адрес(по прописке) Область/Район")]
        public string DistrictPP { get; set; }


        [Display(Name = "Адрес (по прописке) Нас. пункт")]
        public string Nas_Punkt_PP { get; set; }


        [Display(Name = "Адрес (по прописке) Улица,дом")]
        public string Fact_Adress_PP { get; set; }


        [Display(Name = "Адрес (фактический) Область")]
        public int? RegionId { get; set; }
        public Region Region { get; set; }


        [Display(Name = "Адрес (фактический) Район")]
        public int? DictDistrictId { get; set; }
        public DictDistrict DictDistrict { get; set; }


        [Display(Name = "Адрес (фактический) Нас. пункт")]
        public string Nas_Punkt { get; set; }


        [Display(Name = "Адрес (фактический) Улица,дом")]
        public string Fact_Adress { get; set; }


        [Display(Name = "Номер телефона")]
        public string Phone_Number { get; set; }


        [Display(Name = "Доп номер телефона")]
        public string Dop_Phone_Number { get; set; }


        [Display(Name = "Место работы")]
        public string MestoRaboty  { get; set; }

            

        public List<Invalidnost> Invalidnost { get; set; }
        public List<Napravleniy> Napravleniy { get; set; }
        public List<Dannye_Proteza> Dannye_Proteza { get; set; }
        public List<Zakazy> Zakazy { get; set; }
    }

    // Таблица о инвалидности
    public class Invalidnost 
    {
        public int Id { get; set; }

        [Display(Name = "Личное дело ")]
        public int LichnoeDeloId { get; set; }
        public LichnoeDelo LichnoeDelo { get; set; }


        [Display(Name = "Категория инвалидности")]
        [Required(ErrorMessage = "Обязательное поле")]
        public int? CategoriaInvId { get; set; }
        public CategoriaInv CategoriaInv { get; set; }


        [Display(Name = "Группа  инвалидности")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string GroupDisability { get; set; }


        [Display(Name = "Причина   инвалидности")]
        public int? PrichinaInvId { get; set; }
        public PrichinaInv PrichinaInv { get; set; }


        [Display(Name = "Где и когда оперирован ")]
        public string GKO { get; set; }


        [Display(Name = "Дополнения  ")]
        public string Dopolneniy { get; set; }


        public DateTime? CreateDate { get; set; }
    }


    // Таблица Направления на услуги
    public class Napravleniy 
    {
        public int Id { get; set; }


        [Display(Name = "Личное дело ")]
        public int LichnoeDeloId { get; set; }
        public LichnoeDelo LichnoeDelo { get; set; }


        [Display(Name = "Дата направления ")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? DataN { get; set; }


        [Display(Name = "Диагноз при направлении")]
        public string NapDiagnoz { get; set; }


        [Display(Name = "Учреждение")]
        public string NapUchrej { get; set; }


        [Display(Name = "ФИО Врача")]
        public string NapFIO { get; set; }


        [Display(Name = "Вид услуги")]
        public string NapVid { get; set; }


        public DateTime? CreateDate { get; set; }
    }


    // Таблица Заказы и наряды
    public class Zakazy 
    {
        public int Id { get; set; }


        [Display(Name = "Личное дело ")]
        public int LichnoeDeloId { get; set; }
        public LichnoeDelo LichnoeDelo { get; set; }


        [Display(Name = "Номер заказа")]
        public string Nomer { get; set; }


        [Display(Name = "Дата заказа")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? CreateDate { get; set; }


        [Display(Name = "Тип заказа ")]
        public string TypeZakaza { get; set; }


        [Display(Name = "Госпитализирован ")]
        public string Gospital { get; set; }



        [Display(Name = "Срочность")]
        public int? StatusZId { get; set; }
        public StatusZ StatusZ { get; set; }


        [Display(Name = "Причина срочности")]
        public string PrichinaSroch { get; set; }


        [Display(Name = "Услуга")]
        public int? UslugaId { get; set; }
        public Usluga Usluga { get; set; }


        [Display(Name = "Дата вызова (Примерка 1)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Vyzova1  { get; set; }


        [Display(Name = "Дата явки: (Примерка 1)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Yavki1  { get; set; }



        [Display(Name = "Дата вызова (Примерка 2)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Vyzova2 { get; set; }


        [Display(Name = "Дата явки: (Примерка 2)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Yavki2 { get; set; }


        [Display(Name = "Дата вызова (Примерка 3)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Vyzova3 { get; set; }


        [Display(Name = "Дата явки: (Примерка 3)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Yavki3 { get; set; }


        [Display(Name = "Дата изготовления ")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Izg { get; set; }


        [Display(Name = "Дата выдачи")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? D_Vydachi { get; set; }


        [Display(Name = "Статус")]
        public int? StatusId { get; set; }
        public StatusZ Status { get; set; }

        public List<ProtezNZ> ProtezNZ { get; set; }
        public List<ObuvNZ> ObuvNZ { get; set; }

    }


    // Таблица Заказы на изготовление протеза
    public class ProtezNZ  
    {
        public int Id { get; set; }


        [Display(Name = "Заказы и наряды ")]
        public int ZakazyId { get; set; }
        public Zakazy Zakazy { get; set; }


        [Display(Name = "Вид изделия")]
        public int? ProteznId { get; set; }
        public Protezn Protezn { get; set; }


        [Display(Name = "Диагноз")]
        public int? DiagnozId { get; set; }
        public Diagnoz Diagnoz { get; set; }


        [Display(Name = "Диагноз(данные)")]
        public int? AmpNId { get; set; }
        public AmpN AmpN { get; set; }


        [Display(Name = "Сторона протеза")]
        public string Storona { get; set; }


        //Таблица изделий(содержит изготавливаемые части протеза)

        [Display(Name = "Наименование п/фабрикатов")]
        public string NamePFabrikat { get; set; }


        [Display(Name = "Шифр полуфабрикатов")]
        public int? ShifrPFabrikatId { get; set; }
        public ShifrPFabrikat ShifrPFabrikat { get; set; }


        [Display(Name = "Размер")]
        public string Size { get; set; }


        [Display(Name = "Количество левый")]
        public int KolL { get; set; }


        [Display(Name = "Количество правый")]
        public int KolP { get; set; }



        public string NamePFabrikat1 { get; set; }

        public int? ShifrPFabrikat1Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat1 { get; set; }

        public int Size1 { get; set; }

        public int KolL1 { get; set; }

        public int KolP1 { get; set; }



        public string NamePFabrikat2 { get; set; }

        public int? ShifrPFabrikat2Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat2 { get; set; }

        public int Size2 { get; set; }

        public int KolL2 { get; set; }

        public int KolP2 { get; set; }



        public string NamePFabrikat3 { get; set; }

        public int? ShifrPFabrikat3Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat3 { get; set; }

        public int Size3 { get; set; }

        public int KolL3 { get; set; }

        public int KolP3 { get; set; }



        public string NamePFabrikat4 { get; set; }

        public int? ShifrPFabrikat4Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat4 { get; set; }

        public int Size4 { get; set; }

        public int KolL4 { get; set; }

        public int KolP4 { get; set; }



        public string NamePFabrikat5 { get; set; }

        public int? ShifrPFabrikat5Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat5 { get; set; }

        public int Size5 { get; set; }

        public int KolL5 { get; set; }

        public int KolP5 { get; set; }



        public string NamePFabrikat6 { get; set; }

        public int? ShifrPFabrikat6Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat6 { get; set; }

        public int Size6 { get; set; }

        public int KolL6 { get; set; }

        public int KolP6 { get; set; }



        public string NamePFabrikat7 { get; set; }

        public int? ShifrPFabrikat7Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat7 { get; set; }

        public int Size7 { get; set; }

        public int KolL7 { get; set; }

        public int KolP7 { get; set; }




        public string NamePFabrikat8 { get; set; }

        public int? ShifrPFabrikat8Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat8 { get; set; }

        public int Size8 { get; set; }

        public int KolL8 { get; set; }

        public int KolP8 { get; set; }



        public string NamePFabrikat9 { get; set; }

        public int? ShifrPFabrikat9Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat9 { get; set; }

        public int Size9 { get; set; }

        public int KolL9 { get; set; }

        public int KolP9 { get; set; }



        public string NamePFabrikat10 { get; set; }

        public int? ShifrPFabrikat10Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat10 { get; set; }

        public int Size10 { get; set; }

        public int KolL10 { get; set; }

        public int KolP10 { get; set; }



        public string NamePFabrikat11 { get; set; }

        public int? ShifrPFabrikat11Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat11 { get; set; }

        public int Size11 { get; set; }

        public int KolL11 { get; set; }

        public int KolP11 { get; set; }



        public string NamePFabrikat12 { get; set; }

        public int? ShifrPFabrikat12Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat12 { get; set; }

        public int Size12 { get; set; }

        public int KolL12 { get; set; }

        public int KolP12 { get; set; }



        public string NamePFabrikat13 { get; set; }

        public int? ShifrPFabrikat13Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat13 { get; set; }

        public int Size13 { get; set; }

        public int KolL13 { get; set; }

        public int KolP13 { get; set; }



        public string NamePFabrikat14 { get; set; }

        public int? ShifrPFabrikat14Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat14 { get; set; }

        public int Size14 { get; set; }

        public int KolL14 { get; set; }

        public int KolP14 { get; set; }



        public string NamePFabrikat15 { get; set; }

        public int? ShifrPFabrikat15Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat15 { get; set; }

        public int Size15 { get; set; }

        public int KolL15 { get; set; }

        public int KolP15 { get; set; }



        public string NamePFabrikat16 { get; set; }

        public int? ShifrPFabrikat16Id { get; set; }
        public ShifrPFabrikat ShifrPFabrikat16 { get; set; }

        public int Size16 { get; set; }

        public int KolL16 { get; set; }

        public int KolP16 { get; set; }


        public DateTime? CreateDate { get; set; }

    }

    //Таблица Блок Данные по протезированию
    public class Dannye_Proteza  
    {
        public int Id { get; set; }


        [Display(Name = "Личное дело ")]
        public int LichnoeDeloId { get; set; }
        public LichnoeDelo LichnoeDelo { get; set; }


        [Display(Name = "Длина культи")]
        public string DlinaKulti { get; set; }


        [Display(Name = "Форма культи")]
        public int? FormaKultiId { get; set; }
        public FormaKulti FormaKulti { get; set; }


        [Display(Name = "Подвижность культи")]
        public int? PodvijnostKultiId { get; set; }
        public PodvijnostKulti PodvijnostKulti { get; set; }


        [Display(Name = "Рубец")]
        public int? RubezId { get; set; }
        public Rubez Rubez { get; set; }


        [Display(Name = "Состояние кожного покрова и мягких тканей культи")]
        public int? SostKojPokrId { get; set; }
        public SostKojPokr SostKojPokr { get; set; }


        [Display(Name = "Костный опил")]
        public int? KostOpilId { get; set; }
        public KostOpil KostOpil { get; set; }


        [Display(Name = "Опорность культи")]
        public string OpornostKulti { get; set; }


        [Display(Name = "Обьективные данные")]
        public string ObjectivnyeDannye { get; set; }


        public DateTime? CreateDate { get; set; }
    }

    // Таблица Заказ на изготовление обуви
    public class ObuvNZ
    {
        public int Id { get; set; }


        [Display(Name = "Заказы и наряды ")]
        public int ZakazyId { get; set; }
        public Zakazy Zakazy { get; set; }


       
        [Display(Name = "Диагноз")]
        public int? DiagnozId { get; set; }
        public Diagnoz Diagnoz { get; set; }


        [Display(Name = "Диагноз(данные)")]
        public int? AmpNId { get; set; }
        public AmpN AmpN { get; set; }


        [Display(Name = "Модель ")]
        public int? ObyvId { get; set; }
        public Obyv Obyv { get; set; }


        [Display(Name = "Цвет изделия")]
        public int? SvetIzdeliaId { get; set; }
        public SvetIzdelia SvetIzdelia { get; set; }
     

        [Display(Name = "Материал")]
        public string Material { get; set; }


        [Display(Name = "Высота каблука")]
        public string VysotaKab { get; set; }


        [Display(Name = "Материал  каблука")]
        public string MaterialKab { get; set; }


        public DateTime? CreateDate { get; set; }
    }


    //Таблица 	Основные технические операции
    public class Table_Tex_Op
    {

        public int Id { get; set; }


        [Display(Name = "Заказ на изготовление обуви ")]
        public int ObuvNZId { get; set; }
        public ObuvNZ ObuvNZ { get; set; }



        [Display(Name = "Перечень основных операций")]
        public string Operation { get; set; }


        [Display(Name = "ФИО исполнителя")]
        public string FioIspol { get; set; }


        [Display(Name = "Дата исполнения ")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol { get; set; }


        [Display(Name = "Отметка ОТК")]
        public string Otmetka { get; set; }





        public string Operation1 { get; set; }
        public string FioIspol1 { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol1 { get; set; }
        public int Otmetka1 { get; set; }



        public string Operation2 { get; set; }
        public string FioIspol2 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol2 { get; set; }
        public int Otmetka2 { get; set; }


        public string Operation3 { get; set; }
        public string FioIspol3 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol3 { get; set; }
        public int Otmetka3 { get; set; }


        public string Operation4 { get; set; }
        public string FioIspol4 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol4 { get; set; }
        public int Otmetka4 { get; set; }


        public string Operation5 { get; set; }
        public string FioIspol5 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol5 { get; set; }
        public int Otmetka5 { get; set; }



        public string Operation6 { get; set; }
        public string FioIspol6 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol6 { get; set; }
        public int Otmetka6 { get; set; }



        public string Operation7 { get; set; }
        public string FioIspol7 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol7 { get; set; }
        public int Otmetka7 { get; set; }


        public string Operation8 { get; set; }
        public string FioIspol8 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol8 { get; set; }
        public int Otmetka8 { get; set; }



        public string Operation9 { get; set; }
        public string FioIspol9 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol9 { get; set; }
        public int Otmetka9 { get; set; }



        public string Operation10 { get; set; }
        public string FioIspol10 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol10 { get; set; }
        public int Otmetka10 { get; set; }



        public string Operation11 { get; set; }
        public string FioIspol11 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd}")]
        public DateTime? DateIspol11 { get; set; }
        public int Otmetka11 { get; set; }



        public DateTime? CreateDate { get; set; }
    }


   
   
    public enum Protez
    {
        Левый_протез, Правый_протез
    }
    public enum Vybor
    {
        Да, Нет
    }

    public enum StatusZakaza
    {
        Обычный, Срочный
    }
    public enum TipUsl
    {
        Заказ, Наряд_на_Ремонтx
    }
    public enum Usl
    {
        Платная, Бесплатная
    }
    public class PersonPassport
    {
        public string pin { get; set; }
        public string surname { get; set; }
        public string name { get; set; }
        public string nationality { get; set; }
        public DateTime dateOfBirth { get; set; }
        public string passportSeries { get; set; }
        public string passportNumber { get; set; }
        public string voidStatus { get; set; }
        public string passportAuthority { get; set; }
        public DateTime issuedDate { get; set; }
        public DateTime expiredDate { get; set; }
        public string familyStatus { get; set; }
        public string gender { get; set; }
        public string addressRegion { get; set; }
        public string addressLocality { get; set; }
        public string addressStreet { get; set; }
        public string addressHouse { get; set; }
    }


    public class PersonMSEC
    {
        public string OrganizationName { get; set; }
        public string ExaminationDate { get; set; }
        public string ExaminationType { get; set; }
        public string DisabilityGroup { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string TimeOfDisability { get; set; }
        public string ReExamination { get; set; }
        public string StatusCode { get; set; }
    }
   

 
}