﻿using Microsoft.EntityFrameworkCore;
using rupoi.Data.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using rupoi.Models;

namespace rupoi.Data
{
    public class RupoiContext : DbContext
    {
        public RupoiContext(DbContextOptions<RupoiContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }


        //Справочники
        public DbSet<Region> Region { get; set; }
        public DbSet<DictDistrict> DictDistrict { get; set; }
        public DbSet<Usluga> Usluga { get; set; }
        public DbSet<TipUslugi> TipUslugi { get; set; }
        public DbSet<StatusZ> StatusZ { get; set; }
        //public DbSet<Seria> Seria  { get; set; }

        //Диагноз
        public DbSet<Diagnoz> Diagnoz { get; set; }

        public DbSet<AmpN> AmpN { get; set; }

        public DbSet<Otto> Otto { get; set; }
        public DbSet<Protezn> Protezn { get; set; }
        public DbSet<Obyv> Obyv { get; set; }

        //при ампутации
        public DbSet<KostOpil> KostOpil { get; set; }

        //Описание изделия
        public DbSet<OpisIzdelia> OpisIzdelia { get; set; }
        public DbSet<ShinoKoj> ShinoKoj { get; set; }
        public DbSet<Derevyin> Derevyin { get; set; }
        public DbSet<Golen> Golen { get; set; }
        public DbSet<FiksApp> FiksApp { get; set; }
        public DbSet<ProtezGoleni> ProtezGoleni { get; set; }

        //при ампутации
        public DbSet<PodvijnostKulti> PodvijnostKulti { get; set; }
        public DbSet<Rubez> Rubez { get; set; }
        public DbSet<SostKojPokr> SostKojPokr { get; set; }
        public DbSet<FormaKulti> FormaKulti { get; set; }

        // при распечатке Если ортобувь
        public DbSet<VidKrep> VidKrep { get; set; }
        public DbSet<VidPadoshvy> VidPadoshvy { get; set; }

        // описание изделия
        public DbSet<OpisanieIzdelia> OpisanieIzdelia { get; set; }
        public DbSet<KJKsOb> KJKsOb { get; set; }
        public DbSet<KJKsVnut> KJKsVnut { get; set; }
        public DbSet<MetalPlastik> MetalPlastik { get; set; }
        public DbSet<UvelichRazmery> UvelichRazmery { get; set; }
        public DbSet<UkorochenieNK> UkorochenieNK { get; set; }

        // при распечатке Если ортобувь++
        public DbSet<SvetIzdelia> SvetIzdelia { get; set; }
        public DbSet<ObuvNZ> ObuvNZ { get; set; }

        // при распечатке Если протез
        public DbSet<NamePFabrikat> NamePFabrikat { get; set; }
        public DbSet<ShifrPFabrikat> ShifrPFabrikat { get; set; }

        //Справочники

        //Категория инвалидности
        public DbSet<CategoriaInv> CategoriaInv { get; set; }

        //Причина инвалидности 
        public DbSet<PrichinaInv> PrichinaInv { get; set; }
        public DbSet<AgeGroup> AgeGroup { get; set; }



        //Личное дело 
        public DbSet<LichnoeDelo> LichnoeDelo { get; set; }
        public DbSet<Invalidnost> Invalidnost { get; set; }
        public DbSet<Napravleniy> Napravleniy { get; set; }
        public DbSet<Zakazy> Zakazy { get; set; }
        public DbSet<ProtezNZ> ProtezNZ { get; set; }
        public DbSet<Dannye_Proteza> Dannye_Proteza { get; set; }


        public DbSet<User> Users { get; set; }
        public DbSet<DiagnosisType> DiagnosisType { get; set; }
        public DbSet<ScarTypes> ScarTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {   
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Cart>().ToTable("Cart");
            modelBuilder.Entity<BoneDustTypes>().ToTable("BoneDustTypes");
            modelBuilder.Entity<DiagnosisType>().ToTable("DiagnosisType");
            modelBuilder.Entity<ProductType>().ToTable("ProductType");
            modelBuilder.Entity<ScarTypes>().ToTable("ScarTypes");
            modelBuilder.Entity<SkinConditionTypes>().ToTable("SkinConditionTypes");
            modelBuilder.Entity<StumpForm>().ToTable("StumpForm");
            modelBuilder.Entity<DirectToService>().ToTable("DirectToService");
            modelBuilder.Entity<ProstheticsData>().ToTable("ProstheticsData");
        }
    }
}
