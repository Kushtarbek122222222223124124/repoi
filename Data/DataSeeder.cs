﻿using Microsoft.AspNetCore.Identity;
using rupoi.Models;
using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Data
{
    public class DataSeeder
    {
        public static void SeedData(RupoiContext context)
        {

            try
            {

                //if (!context.Roles.Any())
                //{
                //    IdentityRole role = new IdentityRole("Администратор");
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Администратор", NormalizedName = "АДМИНИСТРАТОР" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Регистратура", NormalizedName = "РЕГИСТРАТУРА" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Медотдел", NormalizedName = "МЕДОТДЕЛ" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Врач_техник", NormalizedName = "ВРАЧ_ТЕХНИК" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Склад", NormalizedName = "СКЛАД" });
                //    context.SaveChanges();
                //}
                //регионы
                if (!context.Region.Any())
                {
                    context.Region.Add(new Region() { Id = 1000001, NameRus = "город Бишкек", NameKyrg = "Бишкек шаары" });
                    context.Region.Add(new Region() { Id = 1000002, NameRus = "город Ош", NameKyrg = "Ош шаары" });
                    context.Region.Add(new Region() { Id = 1000003, NameRus = "	Баткенская область", NameKyrg = "Баткен облусу" });
                    context.Region.Add(new Region() { Id = 1000004, NameRus = "	Джалал-Абадская область", NameKyrg = "Жалал-Абад облусу" });
                    context.Region.Add(new Region() { Id = 1000005, NameRus = "Иссык-Кульская область", NameKyrg = "Ысык-Көл облусу" });
                    context.Region.Add(new Region() { Id = 1000006, NameRus = "	Нарынская область", NameKyrg = "Нарын облусу" });
                    context.Region.Add(new Region() { Id = 1000007, NameRus = "Ошская область", NameKyrg = "Ош облусу" });
                    context.Region.Add(new Region() { Id = 1000008, NameRus = "Таласская область", NameKyrg = "Талас облусу" });
                    context.Region.Add(new Region() { Id = 1000009, NameRus = "	Чуйская область", NameKyrg = "Чүй облусу" });
           
                    context.SaveChanges();
                }
                if (!context.DictDistrict.Any())
                {
                    //город Бишкек
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Октябрьский район" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Первомайский район" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Свердловский район" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Ленинский район" });
                   
                    //Баткенская область
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Баткен району", NameRus = "Баткенский район" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Кадамжай району", NameRus = "Кадамжайский район" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Лейлек району", NameRus = "Лейлекский район" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Баткен шаары", NameRus = "город Баткен" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Кызылкыя шаары", NameRus = "	город Кызыл-Кия" }); 
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "город Сулюкта", NameRus = "	Сүлүктү шаары" });

                    //Джалал-Абадская область
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Аксы району", NameRus = "Аксыйский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Ала-Бука району", NameRus = "Ала-Букинский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Базар-Коргон району", NameRus = "Базар-Коргонский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Ноокен району", NameRus = "	Ноокенский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Сузак району", NameRus = "	Сузакский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Тогуз-Торо району", NameRus = "	Тогуз-Тороуский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Токтогул району", NameRus = "Токтогульский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Чаткал району", NameRus = "Чаткальский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Жалал-Абад шаары", NameRus = "город Джалал-Абад" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Кара-Көл шаары", NameRus = "город Кара-Куль" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Майлуу-Суу шаары", NameRus = "город Майлуу-Суу" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Таш-Көмүр шаары", NameRus = "город Таш-Кумыр" });

                    //Иссык-Кульская область
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Ак-Суу району", NameRus = "Ак-Суйский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Жети-Өгүз району", NameRus = "Джети-Огузский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Ысык-Көл району", NameRus = "Иссык-Кульский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Тоң району", NameRus = "Тонский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Түп району", NameRus = "Тюпский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Балыкчы шаары", NameRus = "город Балыкчи" });
                    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Каракол шаары", NameRus = "город Каракол" });

                    //Нарынская область
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Ак-Талаа району", NameRus = "Ак-Талинский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Ат-Башы району", NameRus = "Ат-Башинский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Жумгал району", NameRus = "Жумгальский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Кочкор району", NameRus = "Кочкорский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Нарын району", NameRus = "	Нарынский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Нарын шаары", NameRus = "город Нарын" });

                    //Ошская область
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Алай району", NameRus = "	Алайский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Араван району", NameRus = "Араванский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Кара-Кулжа району", NameRus = "Кара-Кульджинский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Кара-Суу району", NameRus = "Кара-Сууский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Ноокат району", NameRus = "Ноокатский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = " Өзгөн району", NameRus = "Узгенский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Чоң Алай району", NameRus = "Чон-Алайский район" });

                    //Таласская область
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Бакай-Ата району", NameRus = "Бакай-Атинский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Кара-Буура району", NameRus = "Кара-Бууринский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Манас району", NameRus = "	Манасский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Талас району", NameRus = "Таласский район" });
                   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Талас шаары", NameRus = "город Талас" });

                    //Чуйская область
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Аламүдүн району", NameRus = "Аламудунский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Жайыл району", NameRus = "Жайыльский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Кемин району", NameRus = "Кеминский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Москва району", NameRus = "Московский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Панфилов району", NameRus = "Панфиловский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Сокулук району", NameRus = "Сокулукский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Чүй району", NameRus = "Чуйский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Ысык-Ата району", NameRus = "Ысык-Атинский район" });
                  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Токмок шаары", NameRus = "город Токмак" });

                    context.SaveChanges();
                }
                if (!context.Usluga.Any())
                {
                    context.Usluga.Add(new Usluga() { Id=1, NameRus = "Платная" });
                    context.Usluga.Add(new Usluga() { Id = 2, NameRus = "Бесплатная" });
                   context.SaveChanges();
                }
               
                if (!context.TipUslugi.Any())
                {
                    context.TipUslugi.Add(new TipUslugi() { Id = 1, NameRus = "Заказ" });
                    context.TipUslugi.Add(new TipUslugi() { Id = 2, NameRus = "Ремонт" });
                    context.SaveChanges();
                }
                //	Статус заказа: Срочный/Обычный 
                if (!context.StatusZ.Any())
                {
                    context.StatusZ.Add(new StatusZ() { Id = 10001, NameRus = "Срочный" });
                    context.StatusZ.Add(new StatusZ() { Id = 10002, NameRus = "Обычный" });
                    context.SaveChanges();
                }
                //Категория инвалидности 
                if (!context.CategoriaInv.Any())
                {
                    context.CategoriaInv.Add(new CategoriaInv() { NameRus = "ЛОВЗ до 18 лет" });
                    context.CategoriaInv.Add(new CategoriaInv() { NameRus = "ЛОВЗ с детства" });
                    context.CategoriaInv.Add(new CategoriaInv() { NameRus = "Инвалид ВОВ" });
                    context.CategoriaInv.Add(new CategoriaInv() { NameRus = "Инвалид советской армии " });
                    context.CategoriaInv.Add(new CategoriaInv() { NameRus = "Инвалид труда " });
                    context.SaveChanges();
                }
                //Причина инвалидности 
                if (!context.PrichinaInv.Any())
                {
                    context.PrichinaInv.Add(new PrichinaInv() { NameRus = "Травма" });
                    context.PrichinaInv.Add(new PrichinaInv() { NameRus = "Врожденный" });
                    context.PrichinaInv.Add(new PrichinaInv() { NameRus = "Заболевание" });
                    context.SaveChanges();
                }
                //Возрастная группа
                if (!context.AgeGroup.Any())
                {
                    context.AgeGroup.Add(new AgeGroup() { NameRus = "малодетсткое", NameKyrg = "малодетсткое" });
                    context.AgeGroup.Add(new AgeGroup() { NameRus = "детское", NameKyrg = "детское" });
                    context.AgeGroup.Add(new AgeGroup() { NameRus = "подростковое", NameKyrg = "подростковое" });
                    context.AgeGroup.Add(new AgeGroup() { NameRus = "взрослое", NameKyrg = "взрослое" });
                    context.SaveChanges();
                }
                //--диагноз--
                if (!context.Diagnoz.Any())
                {
                    context.Diagnoz.Add(new Diagnoz() { Id = 1000003, NameRus = "ОртОбувь " });
                    context.Diagnoz.Add(new Diagnoz() { Id = 1000002, NameRus = "Протезирование (Ампутация нижних конечностей)  " });
                    context.Diagnoz.Add(new Diagnoz() { Id = 1000001, NameRus = "Протезирование (Ампутация верхних конечностей) " });
                    context.SaveChanges();
                }

                //--диагноз--Диагноз(данные)
                if (!context.AmpN.Any())
                {
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = "ДЦП" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = "косолапость правосторонняя " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = "косолапость левосторонняя" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = " Укорочение левой нижней конечности" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = " Укорочение правой нижней конечности" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = "Деформация пальцев стоп левый" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = " Деформация пальцев стоп правый" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = "  Левая Слоновость" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = "Правая Слоновость" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = " Левый порез " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = "Правый порез " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = " Левая конская стопа" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000003, NameRus = " Правая конская стопа" });

                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " левого бедра " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " правого бедра" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " левой голени " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " правой голени " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " обеих бедер " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " Обеих голеней" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " левого тазобедренного сустава" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " правого тазобедренного сустава " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " Левого По Пирогову " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " правого по Пирогову" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " Правого по Шопару" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " левого по Шопару" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " Левого по Лесфранку" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " правого по Лесфранку " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " Обеих по Шопару " });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000002, NameRus = " обеих по Лесфранку " });

                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "левого плеча" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "правого плеча" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "обеих плеч" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "левого предплечия" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "правого предплечья" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "обеих предплечий" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "левой кисти" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "правой кисти" });
                    context.AmpN.Add(new AmpN() { DiagnozId = 1000001, NameRus = "обеих кистей" });
                    context.SaveChanges();
                }
              
                //виды изделий-- 
                //if (!context.VidIzdelia.Any())
                //{
                //    context.VidIzdelia.Add(new VidIzdelia() { Id = 1000004, NameRus = "Протезный" });
                //    context.VidIzdelia.Add(new VidIzdelia() { Id = 1000005, NameRus = "Обувной" });
                //    context.VidIzdelia.Add(new VidIzdelia() { Id = 1000006, NameRus = "ОттоБок" });
                //    context.SaveChanges();
                //}

                //виды изделий-- Протезный
                if (!context.Protezn.Any())
                {
                    context.Protezn.Add(new Protezn() {  Shifr = "ПРО - 03", NameRus = "  протез кисти с манжеткой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПРО - 12", NameRus = "     протезы после ампутации кисти в пределах пястья хлорвиниловый" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПРО - 12", NameRus = "   протезы после ампутации кисти в пределах пястья из пластмассы" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 2 - 09", NameRus = "  протез предплечья на короткую культю" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 2 - 12", NameRus = "  протез предплечья рабочий" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 2 - 17", NameRus = "   протез предплечья пластмассовый с кожаной манжеткой на плечо" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 2 - 18 ", NameRus = "  протез предплечья косметический крепление уздечкой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 2 - 18", NameRus = "   протез предплечья с кожаной манжеткой на плечо" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 2 - 30", NameRus = " протез предплечья с тяговым управлением и мышечной ротацией крепление уздечкой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 2 - 33", NameRus = " протез предплечья для детей до 6 лет" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 16", NameRus = "  протез плеча рабочий" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 22", NameRus = "  протез плеча пластмассовый" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 23", NameRus = "  протез плеча косметический" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 25", NameRus = "     протез плеча на длинную культю пластмассовый" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 28", NameRus = "     протез плеча рабочий" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 34", NameRus = "    протез плеча функционально - косметический с кожаной гильзой плеча" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 34", NameRus = "    протез плеча функционально - косметический с пластмассовой гильзой плеча" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 4 - 34", NameRus = "     протез плеча функционально - косметический с ламинированной гильзой плеча" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 8 - 02", NameRus = "    протез после вычленения плеча косметический" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 8 - 04", NameRus = "    протез после вычленения плеча, лопатки, и ключицы косметический" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 8 - 06", NameRus = "   протез после вычленения плеча  пластмассовый с полиэтиленовой гильзой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 8 - 06", NameRus = "   протез после вычленения плеча пластмассовый с нитролаковой гильзой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПР 8 - 07", NameRus = "    протез после вычленения плеча для детей до 6 лет" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 01", NameRus = "    протез голени шинно - кожаный(культя по Пирогову) без манжетки" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 01", NameRus = "    протез голени шинно - кожаный(культя по Пирогову) с манжеткой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 05", NameRus = "    протез голени деревянный(культя по Пирогову)" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 12", NameRus = "    протез голени шинно - кожаный" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 13", NameRus = "    протез голени шинно - кожаный с сидением" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 14", NameRus = "     протез голени шинно - кожаный" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 22", NameRus = "     протез голени на согнутое колено на 23П узел" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 22", NameRus = "    протез голени на согнутое колено шинно-кожаный" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 27", NameRus = "    протез голени на согнутое колено без стопы шинно-кожаный" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 41", NameRus = "   протез голени с глубокой посадкой деревянный без гильзы бедра" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 41", NameRus = "   протез голени деревянный с кожаной гильзой бедра" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 42", NameRus = "     протез голени с глубокой посадкой и эластичной облицовкой с деревянным приемником на 23 П узел" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 3 - 42", NameRus = "    протез голени с глубокой посадкой с кожаной гильзой бедра и деревянным вкладышем на 23 П узел" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 - 12", NameRus = "    протез подставка после двухсторонней ампутации бедра с деревянным приемником" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 - 12", NameRus = "    протез подставка после двухсторонней ампутации бедра с кожаным приемником" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 - 20", NameRus = "  протез бедра без стопы с кожаным приемником" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 - 20", NameRus = "   протез бедра  без стопы с деревянным приемником" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 - 35", NameRus = "     протез бедра универсального назначения с деревянным приемником" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 - 35", NameRus = "    протез бедра универсального назначения немецкий с деревянным приемником" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 – 36", NameRus = "     протез бедра на опорную культю с эластичной облицовкой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПН 6 – 36 - 1", NameRus = "   протез бедра на опорную культю с эластичной облицовкой  на 23 узел" });
                    context.Protezn.Add(new Protezn() {  Shifr = "АН 8 - 01", NameRus = "    аппарат на всю ногу шинно - кожаный" });
                    context.Protezn.Add(new Protezn() {  Shifr = "АН 8 – 01Д", NameRus = "  аппарат на всю ногу шинно-кожаный детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "АН 8 - 01", NameRus = "    аппарат на всю ногу из полиэтилена" });
                    context.Protezn.Add(new Protezn() {  Shifr = "АН 8 - 04", NameRus = "     аппарат на всю ногу" });
                    context.Protezn.Add(new Protezn() {  Shifr = "АН 8 - 06", NameRus = "     аппарат на всю ногу с полукорсетом" });
                    context.Protezn.Add(new Protezn() {  Shifr = "АН 8 - 07", NameRus = "     аппарат на всю ногу с двойным следом" });
                    context.Protezn.Add(new Protezn() {  Shifr = "АН 8 – 07Д", NameRus = "   аппарат на всю ногу с двойным следом детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТРО - 03", NameRus = "    Тутор на лучезапястный сустав из полевика(кистедержатель)" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТРО - 02", NameRus = "   Тутор на лучезапястный сустав из полиэтилена(кистедержатель)" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТРО - 02Д", NameRus = "  Тутор на лучезапястный сустав из полиэтилена детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТР2 - 03", NameRus = "   Тутор на предплечье из полевика" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТР2 - 03Д", NameRus = "  Тутор на предплечье из полевика детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТНО - 03", NameRus = "     тутор на голеностопный сустав из полевика" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТНО – 03Д", NameRus = "  тутор на голеностопный сустав из полевика детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТНО - 02", NameRus = "      тутор на голеностопный сустав из полиэтилена" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТНО – 02Д", NameRus = "  тутор на голеностопный сустав из полиэтилена детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 2 - 03", NameRus = "     тутор на голень косметический из полевика" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 2 – 03Д", NameRus = "   тутор на голень косметический из полевика детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 2 - 04", NameRus = "   тутор на голень косметический из полиэтилена" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 4 - 02 - 01", NameRus = "      тутор из полиэтилена с обтяжкой педилином и креплением ремешками" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 4 - 02 - 01Д", NameRus = "   тутор из полиэтилена с обтяжкой педилином и креплением ремешками детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 4 - 02", NameRus = "    тутор на коленный сустав с захватом голени и бедра из полиэтилена" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН4 – 02 Д", NameRus = "   тутор на коленный сустав с захватом голени и бедра из полиэтилена детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН4 - 03Д", NameRus = "   тутор на коленный сустав из полевика детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 6 - 02", NameRus = "     тутор на тазобедренный сустав из полиэтилена" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 8 - 02", NameRus = "     тутор на всю ногу ш / к" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 8 – 02Д", NameRus = "   тутор на всю ногу ш/ к детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 8 - 02", NameRus = "     тутор на всю ногу из " });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 8 – 02Д", NameRus = "   тутор на всю ногу из полиэтилена детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 8 – 02Д", NameRus = "   тутор на всю ногу из полиэтилена с обтяжкой из ортофома детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ТН 8 - 04", NameRus = "     тутор на всю ногу с полукорсетом ш / к" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО - 02", NameRus = "   Корсет головодержатель из полиэтилена" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО - 02 Д", NameRus = "   Корсет головодержатель из полиэтилена детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО - 14", NameRus = "     Корсет текстильный(Ленинградского типа)" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО – 14Д", NameRus = "  Корсет текстильный(Ленинградского     типа) детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО - 19", NameRus = "      Корсет шинно-кожаный на поясничный отдел позвоночника" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО - 24", NameRus = "     Корсет на нижне - грудной отдел позвоночника из полиэтилена" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО – 24Д", NameRus = "   Корсет на нижне-грудной отдел позвоночника из полиэтилена детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КРО - 25", NameRus = "     Корсет шинно-кожаный на средне-грудной отдел позвоночника" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КР 1 - 01", NameRus = "     Реклинатор - текстильный" });
                    context.Protezn.Add(new Protezn() {  Shifr = "КР 1 – 01Д", NameRus = "   Реклинатор -текстильный детский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПИО - 04", NameRus = "      Фрейка(ЦИТО)" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПИО - 05", NameRus = "     шина Виленского" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПИО - 06", NameRus = "     шина Жумабекова" });
                    context.Protezn.Add(new Protezn() {  Shifr = "БН3 - 01,   БН3 - 02", NameRus = "    бандаж при опущений и заболевании почек, послеоперационный, при грыжах и опущении органов брюшной полости, при искуственном анусе, мужской, женский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "БН 2 - 01", NameRus = "     бандаж дородовой и послеродовой" });
                    context.Protezn.Add(new Protezn() {  Shifr = "БН 3 - 01", NameRus = "     бандаж при опущении и заболевании почек, послеоперационный (Российская ткань)" });
                    context.Protezn.Add(new Protezn() {  Shifr = "БН 3 - 21", NameRus = "    Бандаж - полукорсет, женский" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПИ - 01", NameRus = "  суспензорий на узком поясе" });
                    context.Protezn.Add(new Protezn() {  Shifr = "ПИ - 02", NameRus = "  суспензорий на широком поясе" });
                    context.Protezn.Add(new Protezn() {  Shifr = "    ", NameRus = "     Обтуратор" });
                    context.Protezn.Add(new Protezn() {  Shifr = "    ", NameRus = "     вытяжной пояс -жилет" });
                    context.Protezn.Add(new Protezn() {  Shifr = "    ", NameRus = "     Петля Глиссона" });
                    context.Protezn.Add(new Protezn() {  Shifr = "    ", NameRus = " костыли" });
                    context.Protezn.Add(new Protezn() {  Shifr = "    ", NameRus = "    трость металлическая" });
                    context.Protezn.Add(new Protezn() {  Shifr = "    ", NameRus = "   малогаборитные инвалидные коляски" });
                    context.Protezn.Add(new Protezn() {  Shifr = "    ", NameRus = "    Индивидуальная малог инв коляска" });
                    context.SaveChanges();
                }


                //виды изделий-- Модель 
                if (!context.Obyv.Any())
                {
                  
                    context.Obyv.Add(new Obyv() { Shifr = "09 ПСНМ", NameRus = "Полусапоги на протез мужские" });
                   context.Obyv.Add(new Obyv() { Shifr = "09 ПСНЖ", NameRus = "Полусапоги на протез женские" });
                   context.Obyv.Add(new Obyv() { Shifr = "09 ПСН2-1М", NameRus = "Полусапоги мужские с супинатором или пронатором или невысокой боковой поддержкой,  выкладкой свода, углублениями в межстелечном слое в местах омозолинности выносом каблука при плоскостопии 3-й степени" });
                   context.Obyv.Add(new Obyv() { Shifr = "09 ПСН2-1Ж", NameRus = "Полусапоги женские с супинатором или пронатором или невысокой боковой поддержкой,  выкладкой свода, углублениями в межстелечном слое в местах омозолинности выносом каблука при плоскостопии 3-й степени" });
                   context.Obyv.Add(new Obyv() { Shifr = "09 ПСН2-2М", NameRus = "Полусапоги при укорочении стопы до 6 см мужские" });
                   context.Obyv.Add(new Obyv() { Shifr = "09 ПСН2-2Ж", NameRus = "Полусапоги при укорочении стопы до 6 см женские" });
                   context.Obyv.Add(new Obyv() { Shifr = "02- К1Мм", NameRus = "Ботинки на протез мужские на меху" });
                   context.Obyv.Add(new Obyv() { Shifr = "02- К1Мт", NameRus = "Ботинки на протез мужские на текстиле" });
                   context.Obyv.Add(new Obyv() { Shifr = "02- К1Мк", NameRus = "Ботинки на протез мужские на кожзаменителе" });
                   context.Obyv.Add(new Obyv() { Shifr = "02- К1Жм", NameRus = "Ботинки на протез женские на меху" });
                   context.Obyv.Add(new Obyv() { Shifr = "  02 - К1Жт ", NameRus = "   Ботинки на протез женские на текстиле" });
                   context.Obyv.Add(new Obyv() { Shifr = " 02 - К1Жк", NameRus = "    Ботинки на протез женские на кожзаменителе" });
                   context.Obyv.Add(new Obyv() { Shifr = " 02 - К 1 МДм", NameRus = " Ботинки на протез малодетские на меху" });
                   context.Obyv.Add(new Obyv() { Shifr = "02 - К 1 МДо", NameRus = " Ботинки на протез малодетские на овчине" });
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 1  Дм ", NameRus = "Ботинки на протез детские на меху" });
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 1  ", NameRus = "До Ботинки на протез детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 1  Пм", NameRus = " Ботинки на протез подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 1  По", NameRus = " Ботинки на протез подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К3Мм", NameRus = "     Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К3Мт ", NameRus = "    Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К3Мк ", NameRus = "    Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К3Жм", NameRus = "     Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К3Жт ", NameRus = "    Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К3Жк", NameRus = "     Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом женские на кожзаменителе"});
                     context.Obyv.Add(new Obyv() { Shifr = "02 - К4Мм ", NameRus = "    Ботинки на ортопедический аппарат мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К4Мт  ", NameRus = "   Ботинки на ортопедический аппарат мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К4Мк  ", NameRus = "   Ботинки на ортопедический аппарат мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - КЖм", NameRus = "  Ботинки на ортопедический аппарат женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К4Жт ", NameRus = "    Ботинки на ортопедический аппарат женские текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К4Жк ", NameRus = "    Ботинки на ортопедический аппарат женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 4 МДм", NameRus = "  Ботинки на ортопедический аппарат малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 4 МДо", NameRus = "  Ботинки на ортопедический аппарат малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 4  Дм", NameRus = "  Ботинки на ортопедический аппарат детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 4 До ", NameRus = " Ботинки на ортопедический аппарат детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 4 Пм ", NameRus = " Ботинки на ортопедический аппарат подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 4 По", NameRus = "  Ботинки на ортопедический аппарат подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К6Мм  ", NameRus = "   Ботинки при укрочении стопы от 1 до 3 см мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К6Мт   ", NameRus = "  Ботинки при укрочении стопы от 1 до 3 см мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К6Мк  ", NameRus = "   Ботинки при укрочении стопы от 1 до 3 см мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К6Жм   ", NameRus = "  Ботинки при укрочении стопы от 1 до 3 см женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К6Жт  ", NameRus = "   Ботинки при укрочении стопы от 1 до 3 см женские текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К6Жк ", NameRus = "    Ботинки при укрочении стопы от 1 до 3 см женские  на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 6 МДм ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 6 МДо ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 6 Дм", NameRus = "  Ботинки при укорочении стопы от 1 до 3 см детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 6 До", NameRus = "  Ботинки при укорочении стопы от 1 до 3 см детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 6  Пм ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 6  По ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Мм  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Мт ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Мк ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Жм", NameRus = " Ботинки при укрочениии стопы от 3 до 6 см женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Жт", NameRus = " Ботинки при укрочениии стопы от 3 до 6 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Жк ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 7 МДм", NameRus = "  Ботинки при укорочении стопы от 4 до 6 см малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 7 МДо", NameRus = "  Ботинки при укрочениии стопы от 3 до 6 см малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Дм  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см детские  на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7До  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см детские  на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7Пм  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К7По ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К8Мм ", NameRus = "    Ботинки при укрочениии стопы от 7 до 9 см мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К8Мт ", NameRus = "    Ботинки при укрочениии стопы от 7 до 9 см мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К8Мк  ", NameRus = "   Ботинки при укрочениии стопы от 7 до 9 см мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К8Жм  ", NameRus = "   Ботинки при укрочениии стопы от 7 до 9 см женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К8Жт  ", NameRus = "   Ботинки при укрочениии стопы от 7 до 9 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К8Жк ", NameRus = "    Ботинки при укрочениии стопы от 7 до 9 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К9Мм ", NameRus = "    Ботинки при укорочении стопы 10 до 12 см мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К9Мт ", NameRus = "    Ботинки при укорочении стопы 10 до 12 см мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К9Мк  ", NameRus = "   Ботинки при укорочении стопы 10 до 12 см мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К9Жм  ", NameRus = "   Ботинки при укорочении стопы 10 до 12 см женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К9Жт ", NameRus = "    Ботинки при укорочении стопы 10 до 12 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К9Жк  ", NameRus = "   Ботинки при укорочении стопы 10 до 12 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К10Мм ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К10Мт ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К10Мк ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К10Жм ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К10Жт ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К10Жк ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см женские на кожзаментиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К11Мм", NameRus = " Ботинки при укорочении стопы от 16 до 20 см мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К11Мт  ", NameRus = "  Ботинки при укорочении стопы от 16 до 20 см мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К11Мк ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см мужские на кожзаментиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К11Жм ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К11Жт ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К11Жк ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К13Мм  ", NameRus = "  Ботинки на слоновую стопу мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К13Мт  ", NameRus = "  Ботинки на слоновую стопу мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К13Мк  ", NameRus = "  Ботинки на слоновую стопу мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К13Жм  ", NameRus = "  Ботинки на слоновую стопу на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К13Жт  ", NameRus = "  Ботинки на слоновую стопу на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К13Жк  ", NameRus = "  Ботинки на слоновую стопу на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К14Мм  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К14Мт  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К14Мк ", NameRus = "   Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К14Жм  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К14Жт  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К14Жк  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К15Мм  ", NameRus = "  Ботинки после ампутации ноги по Пирогову мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К15Мт  ", NameRus = "  Ботинки после ампутации ноги по Пирогову мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К15Мк  ", NameRus = "  Ботинки после ампутации ноги по Пирогову мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К15Жм  ", NameRus = "  Ботинки после ампутации ноги по Пирогову женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К15Жт  ", NameRus = "  Ботинки после ампутации ноги по Пирогову женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К15Жк", NameRus = " Ботинки после ампутации ноги по Пирогову женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К16Мм  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К16Мт  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К16Мк  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К16Жм  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К16Жт ", NameRus = "   Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К16Жк  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 16 МДм", NameRus = "  Ботинки после ампутации по Шопару малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 16 МДо", NameRus = "  Ботинки после ампутации по Шопару малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 16  Дм ", NameRus = " Ботинки после ампутации по Шопару детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 16  До", NameRus = "  Ботинки после ампутации по Шопару детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 16 Пм", NameRus = "  Ботинки после ампутации по Шопару подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 16 По ", NameRus = " Ботинки после ампутации по Шопару подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К17Мм ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К17Мт  ", NameRus = "  Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К17Мк ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К17Жм ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К17Жт ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К17Жк  ", NameRus = "  Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 17МДм", NameRus = "  Ботинки после ампутации по Лисфранку или при разной длине стопы малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 17МДо ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 17Дм ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 17До ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 17Пм ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 17По", NameRus = "  Ботинки после ампутации по Лисфранку или при разной длине стопы подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К18Мм ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К18Мт ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К18Мк ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К18Жм  ", NameRus = "  Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К18Жт ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К18Жк ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К21М  ", NameRus = "   Вставной башмачок в обувь(нормальную) после ампутации переднего отдела стопы мужской"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К21Ж  ", NameRus = "   Вставной башмачок в обувь(нормальную) после ампутации переднего отдела стопы женский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 21 МД ", NameRus = " Вставной башмак после ампутации переднего отдела стопы в обувь(нормальную)  малодетский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 21 Д ", NameRus = " Вставной башмак после ампутации переднего отдела стопы в обувь(нормальную) детский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 21 П ", NameRus = " Вставной башмак после ампутации переднего отдела стопы в обувь(нормальную) подростковый"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К23 М ", NameRus = " Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару мужской"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К23Ж  ", NameRus = "   Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару женский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К23 МД ", NameRus = " Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару малодетский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К23 Д ", NameRus = " Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару детский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К23П  ", NameRus = "   Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару подростковый"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К24М  ", NameRus = "   Вставной башмачок с двойным следом в обувь(нормальную) мужской"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К24Ж  ", NameRus = "   Вставной башмачок с двойным следом в обувь(нормальную)  женский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К24МД", NameRus = " Вставной башмачок с двойным следом в обувь(нормальную) малодетский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К24Д  ", NameRus = "   Вставной башмачок с двойным следом в обувь(нормальную) детский"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К24П", NameRus = " Вставной башмачок с двойным следом в обувь(нормальную) подростковый"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К25Мм  ", NameRus = "  Ботинкис металлическими шинами после ампутации стопы по Пирогову мужской на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К25Мт  ", NameRus = "  Ботинкис металлическими шинами после ампутации стопы по Пирогову мужской на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К25Мк ", NameRus = "   Ботинкис металлическими шинами после ампутации стопы по Пирогову мужской на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К25Жм  ", NameRus = " Ботинкис металлическими шинами после ампутации стопы по Пирогову женский на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К25Жт  ", NameRus = "   Ботинкис металлическими шинами после ампутации стопы по Пирогову женский натекстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К25Жк ", NameRus = "    Ботинкис металлическими шинами после ампутации стопы по Пирогову женский на кожзаменитиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 25 МДм", NameRus = "   Ботинки после ампутации голени по Пирогову с металлическими шинами  малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 25 МДо ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами  малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 25  Дм ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 25  До ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 25 Пм", NameRus = "   Ботинки после ампутации голени по Пирогову с металлическими шинами  подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 25 По ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами  подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 1Мм ", NameRus = "  Полуботинки на протез мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 1Мт ", NameRus = "  Полуботинки на протез мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 1Мк", NameRus = "   Полуботинки на протез мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 1Жм ", NameRus = "  Полуботинки на протез женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 1Жт ", NameRus = "  Полуботинки на протез женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 1Жк", NameRus = "   Полуботинки на протез женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К1 МДм", NameRus = "   Полуботинки на протез малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К1 МДо", NameRus = " Полуботинки на протез малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К1 Дм ", NameRus = "Полуботинки на протез детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К1 До ", NameRus = "Полуботинки на протез детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К1 Пм ", NameRus = "Полуботинки на протез подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К1 По ", NameRus = "Полуботинки на протез подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К1 - 1 ", NameRus = "Женские полуботинки на протез"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 3Мм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 3Мт", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 3Мк ", NameRus = "Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 3Жм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 3Жт", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 3Жк", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К3 МДм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К3 МДо ", NameRus = "Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К3 Дм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К3 До", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К3 Пм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К3 По ", NameRus = "Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 3 - 1 ", NameRus = " Женские полуботинки с супинатором или пронатором"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 4Мм ", NameRus = "Полуботинки  на фиксационный аппарат мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 4Мт", NameRus = " Полуботинки  на фиксационный аппарат мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 4Мк ", NameRus = "Полуботинки  на фиксационный аппарат мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 4Жм ", NameRus = "Полуботинки  на фиксационный аппарат женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 4Жт ", NameRus = "Полуботинки  на фиксационный аппарат женские на тектиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "05 - К 4Жк ", NameRus = "Полуботинки  на фиксационный аппарат женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К4 МДм ", NameRus = "Полуботинки на ортопедический аппарат малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К4 МДо", NameRus = " Полуботинки на ортопедический аппарат малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К4 Дм ", NameRus = "Полуботинки на ортопедический аппарат детские меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К4 До ", NameRus = "Полуботинки на ортопедический аппарат детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К4 Пм ", NameRus = "Полуботинки на ортопедический аппарат подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К4 По ", NameRus = "Полуботинки на ортопедический аппарат подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 6 Мм ", NameRus = "Полуботинки при укорочении стопы до 3 см мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 6 Мт ", NameRus = "Полуботинки при укорочении стопы до 3 см мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 6 Мк", NameRus = " Полуботинки при укорочении стопы до 3 см мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 6Жм ", NameRus = "Полуботинки при укорочении стопы до 3 см женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 6Жт", NameRus = " Полуботинки при укорочении стопы до 3 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 6Жк ", NameRus = "Полуботинки при укорочении стопы до 3 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К6 МДм ", NameRus = "Полуботинки при укорочении стопы от 1 до 3 см малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К6 МДо", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К6 Дм ", NameRus = "Полуботинки при укорочении стопы от 1 до 3 см детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К6 До", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К6 Пм", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К6 По", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 6 - 1  ", NameRus = "  Женские полуботинки при укорочении стопы до 3 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 7 Мм ", NameRus = "Полуботинки при укорочении стопы от 3 см и выше мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 7 Мт ", NameRus = "Полуботинки при укорочении стопы от 3 см и выше мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 7 Мк ", NameRus = "Полуботинки при укорочении стопы от 3 см и выше мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 7 Жм", NameRus = " Полуботинки при укорочении стопы от 3 см и выше женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 7 Жт", NameRus = " Полуботинки при укорочении стопы от 3 см и выше женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 7 Жк", NameRus = " Полуботинки при укорочении стопы от 3 см и выше женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К7 МДм ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см малодетские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К7 МДо", NameRus = " Полуботинки при укорочении стопы от 3 до 6 см малодетские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К7 Дм ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см детские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К7 До ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см детские на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К7 Пм ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см подростковые на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К7 По ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см подростковые на овчине"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 7 - 1  Женские", NameRus = " полуботинки с укорочением стопы от 3 см и выше"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 10 Мм", NameRus = " Полуботинки после ампутации стопы по Шопару мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 10 Мт ", NameRus = "Полуботинки после ампутации стопы по Шопару мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 10 Мк ", NameRus = "Полуботинки после ампутации стопы по Шопару мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 10 Жм ", NameRus = "Полуботинки после ампутации стопы по Шопару женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 10 Жт ", NameRus = "Полуботинки после ампутации стопы по Шопару женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 10 Жк ", NameRus = "Полуботинки после ампутации стопы по Шопару женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 11Мм", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 11Мт ", NameRus = "Полуботинки после ампутации стопы по Лисфранку при разной длине следа мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 11Мк", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 11 Жм", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 11 Жт ", NameRus = "Полуботинки после ампутации стопы по Лисфранку при разной длине следа женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 11 Жк", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 13Мм ", NameRus = "Полуботинки на слоновую стопу мужские на меху" });
                   context.Obyv.Add(new Obyv() { Shifr = "    03 - К 13Мт", NameRus = " Полуботинки на слоновую стопу мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 13Мк", NameRus = " Полуботинки на слоновую стопу мужские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 13 Жм", NameRus = "Полуботинки на слоновую стопу женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 13 Жт ", NameRus = "Полуботинки на слоновую стопу женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 13 Жк ", NameRus = "Полуботинки на слоновую стопу женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 14Мм", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 14Мт", NameRus = " Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 14Мк ", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на кожзаменителе на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 14Жм ", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на меху"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 14Жт ", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "03 - К 14Жк", NameRus = " Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 1 Жт", NameRus = " Туфли на протез женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 1 Жк ", NameRus = "Туфли на протез женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К1 - 1", NameRus = " Комплект деталей верха летних туфель с открытым носком или комплект деталей верха сандалет"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 2 Ж т", NameRus = " Туфли на протез на среднем каблуке женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 2 Ж к", NameRus = "  Туфли на протез на среднем каблуке женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 3 Жт", NameRus = " Туфли на протез на высоком каблуке женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 3 Жк ", NameRus = "Туфли на протез на высоком каблуке женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К3 - 1", NameRus = " Туфли типа сандалет на высоком каблуке"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 5", NameRus = "   Туфлина низком каблуке с супинатором, пронатором или невысокой боковой поддержкой"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 6 Жт", NameRus = "Туфли на среднем каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 6 Жк", NameRus = " Туфли на среднем каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 7 Жт ", NameRus = "Туфли на высоком каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 7 Жк", NameRus = " Туфли на высоком каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 8 Жт", NameRus = " Туфли на низком каблуке при укорочении стопы от 1 до 3 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 8 Жк", NameRus = "Туфли на низком каблуке при укорочении стопы от 1 до 3 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К8 - 1,04 - К6,04 - К7, 04 - К9, 04 - К10 ", NameRus = " Туфли типа босоножки при укорочении стопы до 3 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 9 Жт", NameRus = "Туфли на среднем каблуке при укорочении стопы от 1 до 3 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 9 Жк", NameRus = " Туфли на среднем каблуке при укорочении стопы от 1 до 3 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 10Жт ", NameRus = "Туфли на высоком каблуке при укорочении стопы от 1 до 3 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 10 Жк", NameRus = "Туфли на высоком каблуке при укорочении стопы от 1 до 3 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 11 Жт ", NameRus = "Туфли на низком каблуке при укорочении стопы от 3 см до 6 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 11 Жк ", NameRus = "Туфли на низком каблуке при укорочении стопы от 3 см до 6 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К11 - 1 ", NameRus = "   Ортопедические туфли типа босоножки при укорочении стопы от 4 до 6 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 12 Жт", NameRus = " Туфли на среднем каблуке при укорочении стопы от 7 до 9 см женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 12 Жк", NameRus = "Туфли на среднем каблуке при укорочении стопы от 7 до 9 см женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К12 - 1 ", NameRus = "  Ортопедические туфли типа босоножки при укорочении стопы от 7 до 9 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 13 Жт ", NameRus = "Туфли на ортопедический аппарат женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 13 Жк", NameRus = "Туфли на ортопедический аппарат женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 14 Жт", NameRus = "Туфли на низком каблуке на сложно - деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 14 Жк ", NameRus = "Туфли на низком каблуке на сложно - деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 15 Жт ", NameRus = "Туфли на среднем каблуке на сложно деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 15 Жк", NameRus = "Туфли на среднем каблуке на сложно деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 16 Жт ", NameRus = "Туфли на низком каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 16 Жк ", NameRus = "Туфли на низком каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 17 Жт ", NameRus = "Туфли на среднем каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 17 Жк", NameRus = " Туфли на среднем каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 18 Жт ", NameRus = "Туфли на низком каблуке с высоким узким жестким задником женские на текстиле"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К 18 Жк", NameRus = "Туфли на низком каблуке с высоким узким жестким задником женские на кожзаменителе"});
                    context.Obyv.Add(new Obyv() { Shifr = "04 - К20   ", NameRus = "Туфли на низком каблуке на слоновую стопу"});
                    context.Obyv.Add(new Obyv() { Shifr = "013 - К 1 - 1", NameRus = "   Сандалеты на протез"});
                    context.Obyv.Add(new Obyv() { Shifr = "013 - К 3 - 1 ", NameRus = " Сандалеты с супинатором или пронатором"});
                    context.Obyv.Add(new Obyv() { Shifr = "013 - К 6 - 1 ", NameRus = " Сандалеты при укорочении стопы от 1 до 6 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "013 - К 6 - 2 ", NameRus = " Сандалеты при укорочении стопы от 7 до 9 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "014 - К 1, 014 - К 3 ", NameRus = " Тапочки без задника с супинатором или пронатором"});
                    context.Obyv.Add(new Obyv() { Shifr = "014 - К 6", NameRus = "Тапочки без задника при укорочении стопы от 1 до 3 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "014 - К 7", NameRus = "Тапочки без задника при укорочении стопы от 3 до 6 см"});
                    context.Obyv.Add(new Obyv() { Shifr = "ПИО - 02 ", NameRus = " Подколенник"});
                    context.Obyv.Add(new Obyv() { Shifr = "ПИО - 01 ", NameRus = " Кожаные брюки(сиденье кожаное)"});
                    context.Obyv.Add(new Obyv() { Shifr = "ПИО - 03 ", NameRus = " чулки - ползунки взрослые"});
                    context.Obyv.Add(new Obyv() { Shifr = "  ", NameRus = " стельки взрослые" });
                    context.Obyv.Add(new Obyv() { Shifr = "  ", NameRus = "стельки косок" });
                    context.Obyv.Add(new Obyv() { Shifr = "  ", NameRus = " съемный корсет взрослый" });
                    context.Obyv.Add(new Obyv() { Shifr = "АН8 - 07 ", NameRus = " Аппарат на всю ногу с двойным следом"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 8 МД, Д" ,NameRus = "  П Ботинки при укорочении стопы от 7 до 9 см малодетские, детские, подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 9 МД, Д", NameRus = " П Ботинки при укорочении стопы от 10 до 12 см малодетские, детские, подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 10 МД, Д", NameRus = "  П Ботинки при укорочении стопы от 13 до 15 см малодетские, детские, подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 12 П", NameRus = "  Ботинки  с двойным следом подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К13П ", NameRus = " Ботинки на слоновую стопу  подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 14 МД, Д, П ", NameRus = " Ботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) малодетские, детские, подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 15 П Ботинки ", NameRus = " после ампутации голени по Пирогову подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 16 МД, Д, П ", NameRus = " Ботинки после ампутации по Шопару малодетские, детские, подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "02 - К 19 МД, Д, П ", NameRus = " Ботинки  с жестким задником,  задником продленным до носка, с жесткими берцами малодетские, детские, подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "013 - К1 - 1П, 013 - К3 - 1П ", NameRus = " Сандалеты подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "014 - К1П, 014 - К3П   ", NameRus = "  Комнатные тапочки с супинатором или пронатором подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "014 - К6П", NameRus = "  Комнатные тапочки при укорочении стопы до 3 см подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "014 - К7П ", NameRus = " Комнатные тапочки при укорочении стопы от 3 до 6 см подростковые"});
                    context.Obyv.Add(new Obyv() { Shifr = "  ", NameRus =  "стельки детские"});
                    context.Obyv.Add(new Obyv() { Shifr = "  ", NameRus = "стельки косок детский" });
                    context.Obyv.Add(new Obyv() { Shifr = "  ", NameRus = "съемный корсет детский" });





                    context.SaveChanges();
            
                }


                //виды изделий-- Оттобок 
                // context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " модульный протез голени" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " модульный протез голени детский" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " модульный протез  голени(смена приемной гильзы)" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " модульный протез  голени c лайнером" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " тонкостенный протез голени" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " тонкостенный протез голени детский" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Модульный протез бедра" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Модульный протез бедра детский" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Модульный протез бедра(с лайнером)" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Модульный протез бедра смена приемной гильзы(с лайнером)" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Модульный протез бедра(смена приемной гильзы)" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Модульный протез бедра(смена приемной гильзы) детский" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " протез бедра тип 4  на вычленение" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " протез бедра тип 4  на вычленение(смена приемной гильзы)" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Фикс аппарат" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Вкладной башмак по Шопару" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Вкладной башмак по Шопару  детский" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = "Тутор" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Тутор детский" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Головодержатель" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Головодержатель детский" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " корсет" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Корсет коррегирующий" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Стельки" });
                //context.Obyv.Add(new Obyv() { VidIzdeliaId = 1000006, Shifr = "", NameRus = " Стельки детский" });
                // context.SaveChanges();



                //при ампутации--Костный опил
                if (!context.KostOpil.Any())
                {
                    context.KostOpil.Add(new KostOpil() { NameRus = "болезненный" });
                    context.KostOpil.Add(new KostOpil() { NameRus = "безболезненный" });
                    context.KostOpil.Add(new KostOpil() { NameRus = "неровный" });
                    context.KostOpil.Add(new KostOpil() { NameRus = "гладкий" });
                    context.KostOpil.Add(new KostOpil() { NameRus = "остеофиты" });
                    context.SaveChanges();
                }
                //при ампутации--Описание изделия--
                if (!context.OpisIzdelia.Any())
                {
                    context.OpisIzdelia.Add(new OpisIzdelia() { Id = 1000007, NameRus = "Шино-кожный" });
                    context.OpisIzdelia.Add(new OpisIzdelia() { Id = 1000008, NameRus = "деревянный " });
                    context.OpisIzdelia.Add(new OpisIzdelia() { Id = 1000009, NameRus = "Голень" });
                    context.OpisIzdelia.Add(new OpisIzdelia() { Id = 1000010, NameRus = "Фиксационный аппарат" });
                    context.OpisIzdelia.Add(new OpisIzdelia() { Id = 1000011, NameRus = "Протез голени по Пирогову" });
                    context.SaveChanges();
                }
                //при ампутации--Описание изделия--Шино-кожный(все)
                if (!context.ShinoKoj.Any())
                {
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Протез левого бедра, шино-кожный" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Узел, 16 ФЛ с замком " });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Узел, 23 ФЛ с замком" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Узел 7ПЛ" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Узел, 16 ФЛ без замка" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Узел, 23 ФЛ без замка" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Узел 7ПЛ " });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Креплени, гильза бедра на трех ремешках" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Пояс узкий" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Пояс широкий" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Стопа резиновая р.стопа с примеркой" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000007, NameRus = "Стопа резиновая р.стопа без примеркой" });

                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Протез левого бедра, деревянный" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Узел, 16 ФЛ с замком " });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Креплени, гильза бедра на трех ремешках" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Пояс узкий" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Пояс широкий" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Стопа резиновая р.стопа с примеркой " });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000008, NameRus = "Стопа резиновая р.стопа без примеркой" });

                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Протез левой голени, шино-кожный" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Узел, 16 ФЛ с замком " });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Узел, 23 ФЛ с замком" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Крепление, гильза бедра и голени на трех ремешках" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Крепление, гильза бедра и голени на шнурках" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Пояс узкий" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Пояс широкий " });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Кожвертлук " });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Стопа резиновая р.стопа с примеркой" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Стопа резиновая р.стопа без примеркой" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Уздечкой" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Крепление, гильза  голени на шнурках деревянный" });
                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000009, NameRus = "Крепление, гильза  голени на шнурках " });

                  context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "На правую  конечность" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "На нижнюю конечность" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "На нижнюю конечность и на правую конечность" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Шинокожанный" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Полиэтиленовый" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "С сидением" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Без сидения" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Башмачком" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Безбашмачка" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "С замком в коленном шарнире" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Без замка в коленном шарнире" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Движение в голеностопном суставе полное" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Движение в голеностопном суставе частичное" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление узкий" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление широкий " });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Кожвертлук" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление гильза бедра и голени на ремешках" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление гильза бедра и голени на шнуровке" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление гильза бедра и голени на ремешках с примеркой" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление гильза бедра и голени на ремешках без примерки " });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление гильза бедра и голени на шнуровке с примеркой" });
                   context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000010, NameRus = "Крепление гильза бедра и голени на шнуровке без примерки" });

                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Протез левого бедра, шино-кожный" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Чашка " });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Креплени, гильза бедра на трех ремешках" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Пояс узкий" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Пояс широкий" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Стопа резиновая р.стопа с примеркой " });
                    context.ShinoKoj.Add(new ShinoKoj() { OpisIzdeliaId = 1000011, NameRus = "Стопа резиновая р.стопа без примеркой" });
                    context.SaveChanges();
                }
                //при ампутации--Описание изделия--деревянный 
                //if (!context.Derevyin.Any())
                //{
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Протез левого бедра, деревянный" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Узел, 16 ФЛ с замком " });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Креплени, гильза бедра на трех ремешках" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Пояс узкий" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Пояс широкий" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Стопа резиновая р.стопа с примеркой " });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Стопа резиновая р.стопа без примеркой" });
                //    context.SaveChanges();
                //}
                //при ампутации--Описание изделия--Голень
                //if (!context.Golen.Any())
                //{
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Протез левой голени, шино-кожный" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Узел, 16 ФЛ с замком " });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Узел, 23 ФЛ с замком" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза бедра и голени на трех ремешках" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза бедра и голени на шнурках" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Пояс узкий" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Пояс широкий " });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Кожвертлук " });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Стопа резиновая р.стопа с примеркой" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Стопа резиновая р.стопа без примеркой" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Уздечкой" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза  голени на шнурках деревянный" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза  голени на шнурках " });

                //    context.SaveChanges();
                //}
                //при ампутации--Описание изделия-Фиксационный аппарат
                //if (!context.FiksApp.Any())
                //{
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "На правую  конечность" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "На нижнюю конечность" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "На нижнюю конечность и на правую конечность" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Шинокожанный" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Полиэтиленовый" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "С сидением" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Без сидения" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Башмачком" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Безбашмачка" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "С замком в коленном шарнире" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Без замка в коленном шарнире" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Движение в голеностопном суставе полное" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Движение в голеностопном суставе частичное" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление узкий" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление широкий " });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Кожвертлук" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на ремешках" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на шнуровке" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на ремешках с примеркой" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на ремешках без примерки " });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на шнуровке с примеркой" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на шнуровке без примерки" });
                //    context.SaveChanges();
                //}
                //при ампутации--Описание изделия--Протез голени по Пирогову 
                //if (!context.ProtezGoleni.Any())
                //{
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Протез левого бедра, шино-кожный" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Чашка " });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Креплени, гильза бедра на трех ремешках" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Пояс узкий" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Пояс широкий" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Стопа резиновая р.стопа с примеркой " });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Стопа резиновая р.стопа без примеркой" });
                //    context.SaveChanges();
                //}
                //при ампутации--Подвижность культи
                if (!context.PodvijnostKulti.Any())
                {
                    context.PodvijnostKulti.Add(new PodvijnostKulti() { NameRus = "нормальная" });
                    context.PodvijnostKulti.Add(new PodvijnostKulti() { NameRus = "ограничение движения"});
                    context.PodvijnostKulti.Add(new PodvijnostKulti() { NameRus = "контрактура" });

                    context.SaveChanges();
                }
                //при ампутации--Рубец
                if (!context.Rubez.Any())
                {
                    context.Rubez.Add(new Rubez() { NameRus = "линейный" });
                    context.Rubez.Add(new Rubez() { NameRus = "звездчатый" });
                    context.Rubez.Add(new Rubez() { NameRus = "центральный" });
                    context.Rubez.Add(new Rubez() { NameRus = "передний" });
                    context.Rubez.Add(new Rubez() { NameRus = "задний"});
                    context.Rubez.Add(new Rubez() { NameRus = "боковой" });
                    context.Rubez.Add(new Rubez() { NameRus = "подвижный" });
                    context.Rubez.Add(new Rubez() { NameRus = "спаянный" });
                    context.Rubez.Add(new Rubez() { NameRus = "безболезненный" });
                    context.Rubez.Add(new Rubez() { NameRus = "келоидный" });
                    context.SaveChanges();
                }
                //при ампутации--Состояние кожного покрова и мягких тканей культи
                if (!context.SostKojPokr.Any())
                {
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "нормальный" });
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "синюшный" });
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "отечный" });
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "потертости" });
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "трещины" });
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "язвы" });
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "свищи" });
                    context.SostKojPokr.Add(new SostKojPokr() { NameRus = "невромы" });

                    context.SaveChanges();
                }
                //при ампутации--Форма культи
                if (!context.FormaKulti.Any())
                {
                    context.FormaKulti.Add(new FormaKulti() { NameRus = "цилиндрическая" });
                    context.FormaKulti.Add(new FormaKulti() { NameRus = "булавовидная" });
                    context.FormaKulti.Add(new FormaKulti() { NameRus = "умеренно-коническая"});
                    context.FormaKulti.Add(new FormaKulti() { NameRus = "резко-коническая"});
                    context.FormaKulti.Add(new FormaKulti() { NameRus = "избыток ткани" });
                    context.FormaKulti.Add(new FormaKulti() { NameRus = "атрофия" });
                    context.SaveChanges();
                }
                //при распечатке--Если ортобувь--Вид крепления
                if (!context.VidKrep.Any())
                {
                    context.VidKrep.Add(new VidKrep() { NameRus = "на шнуровке" });
                    context.VidKrep.Add(new VidKrep() { NameRus = "на замочках" });
                    context.VidKrep.Add(new VidKrep() { NameRus = "на ремешках"});
                    context.VidKrep.Add(new VidKrep() { NameRus = "на резинках" });
                    context.VidKrep.Add(new VidKrep() { NameRus = "с одним перекидным ремешком" });
                    context.SaveChanges();
                }
                //при распечатке--Если ортобувь--Вид подошвы
                if (!context.VidPadoshvy.Any())
                {
                    context.VidPadoshvy.Add(new VidPadoshvy() { NameRus = "микропористая" });
                    context.VidPadoshvy.Add(new VidPadoshvy() { NameRus = "полиуретановая" });
                    context.SaveChanges();
                }

                //при распечатке--Если ортобувь--описание изделия
                if (!context.OpisanieIzdelia.Any())
                {
                    context.OpisanieIzdelia.Add(new OpisanieIzdelia() { Id = 1000012, NameRus = "круговой жесткий корсет с обеих сторон до носка " });
                    context.OpisanieIzdelia.Add(new OpisanieIzdelia() { Id = 1000013, NameRus = "круговой жесткий корсет с внутренней стороны до носка" });
                    context.OpisanieIzdelia.Add(new OpisanieIzdelia() { Id = 1000014, NameRus = "металлическая пластинка подошвы " });
                    context.OpisanieIzdelia.Add(new OpisanieIzdelia() { Id = 1000015, NameRus = "увеличенные размеры" });
                    context.OpisanieIzdelia.Add(new OpisanieIzdelia() { Id = 1000016, NameRus = "Укорочение нижних конечностей" });
                    context.SaveChanges();
                }
                //при распечатке--Если ортобувь--описание изделия--круговой жесткий корсет с обеих сторон до носка
                if (!context.KJKsOb.Any())
                {
                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000012, NameRus = "выкладка свода " });
                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000012, NameRus = "отведение каблука до конца первого пальца" });
                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000012, NameRus = "отведение каблука до пучков " });
                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000012, NameRus = "отведение каблука до носка" });

                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000013, NameRus = "выкладка свода " });
                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000013, NameRus = "пронатор отведение каблука к наруже до пучка " });
                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000013, NameRus = "пронатор отведение каблука к наруже до носка " });

                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000014, NameRus = "жесткий перед " });
                    context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000014, NameRus = "искусственный носок " });

                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000015, NameRus = "расширенная подошва " });
                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000015, NameRus = "набить сбоку колодки " });
                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000015, NameRus = "набить сверху пальцев " });
                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000015, NameRus = "выкладка свода " });
                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000015, NameRus = "углубить пятку под место омазалости " });

                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000016, NameRus = "пробка под пяту " });
                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000016, NameRus = "выкладка свода " });
                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000016, NameRus = "Круговой жесткий корсет  " });
                     context.KJKsOb.Add(new KJKsOb() { OpisanieIzdeliaId = 1000016, NameRus = "углубить пятку под место омазалости " });


                    context.SaveChanges();
                }
                //при распечатке--Если ортобувь--описание изделия--круговой жесткий корсет с внутренней стороны до носка
                //if (!context.KJKsVnut.Any())
                //{
                //    context.KJKsVnut.Add(new KJKsVnut() { OpisanieIzdeliaId = 13, NameRus = "выкладка свода " });
                //    context.KJKsVnut.Add(new KJKsVnut() { OpisanieIzdeliaId = 13, NameRus = "пронатор отведение каблука к наруже до пучка " });
                //    context.KJKsVnut.Add(new KJKsVnut() { OpisanieIzdeliaId = 13, NameRus = "пронатор отведение каблука к наруже до носка " });

                //    context.SaveChanges();
                //}
                //при распечатке--Если ортобувь--описание изделия--металлическая пластинка подошвы
                //if (!context.MetalPlastik.Any())
                //{
                //    context.MetalPlastik.Add(new MetalPlastik() { OpisanieIzdeliaId = 14, NameRus = "жесткий перед " });
                //    context.MetalPlastik.Add(new MetalPlastik() { OpisanieIzdeliaId = 14, NameRus = "искусственный носок " });
                //    context.SaveChanges();
                //}

                //при распечатке--Если ортобувь--описание изделия--увеличенные размеры
                //if (!context.UvelichRazmery.Any())
                //{
                //    context.UvelichRazmery.Add(new UvelichRazmery() { OpisanieIzdeliaId = 15, NameRus = "расширенная подошва " });
                //    context.UvelichRazmery.Add(new UvelichRazmery() { OpisanieIzdeliaId = 15, NameRus = "набить сбоку колодки " });
                //    context.UvelichRazmery.Add(new UvelichRazmery() { OpisanieIzdeliaId = 15, NameRus = "набить сверху пальцев " });
                //    context.UvelichRazmery.Add(new UvelichRazmery() { OpisanieIzdeliaId = 15, NameRus = "выкладка свода " });
                //    context.UvelichRazmery.Add(new UvelichRazmery() { OpisanieIzdeliaId = 15, NameRus = "углубить пятку под место омазалости " });
                //    context.SaveChanges();
                //}
                //при распечатке--Если ортобувь--описание изделия--Укорочение нижних конечностей
                //if (!context.UkorochenieNK.Any())
                //{
                //    context.UkorochenieNK.Add(new UkorochenieNK() { OpisanieIzdeliaId = 16, NameRus = "пробка под пяту " });
                //    context.UkorochenieNK.Add(new UkorochenieNK() { OpisanieIzdeliaId = 16, NameRus = "выкладка свода " });
                //    context.UkorochenieNK.Add(new UkorochenieNK() { OpisanieIzdeliaId = 16, NameRus = "Круговой жесткий корсет  " });
                //    context.UkorochenieNK.Add(new UkorochenieNK() { OpisanieIzdeliaId = 16, NameRus = "углубить пятку под место омазалости " });
                //    context.SaveChanges();
                //}
                //при распечатке--Если ортобувь--Цвет изделия
                if (!context.SvetIzdelia.Any())
                {
                    context.SvetIzdelia.Add(new SvetIzdelia() {  NameRus = "черный " });
                    context.SvetIzdelia.Add(new SvetIzdelia() {  NameRus = "красный " });
                    context.SvetIzdelia.Add(new SvetIzdelia() {  NameRus = "комбинированный  " });
                    context.SvetIzdelia.Add(new SvetIzdelia() {  NameRus = "бордовый " });
                    context.SaveChanges();
                }
                //при распечатке--Если протез--Наименование полуфабрикатов
                if (!context.NamePFabrikat.Any())
                {
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000017, NameRus = "Шины " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000018, NameRus = "стопа " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000019, NameRus = "щиколотка  " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000020, NameRus = "узел " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000021, NameRus = "вертлуг " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000022, NameRus = "п/кольцо" });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000023, NameRus = "сиденье " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000024, NameRus = "кисть " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000025, NameRus = "стелька " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000026, NameRus = "болванка " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000027, NameRus = "насадка  " });
                    context.NamePFabrikat.Add(new NamePFabrikat() { Id = 1000028, NameRus = "Чашка  " });
                    context.SaveChanges();
                }
                //при распечатке--Если протез--Шифр полуфабрикатов
                if (!context.ShifrPFabrikat.Any())
                {
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "010 шарнир " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "050 шарнир " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "б/шарнир  " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "010 норм " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "016 усл " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "005 В" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "002 без замка " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "008 с замком " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "032 " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "031 " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "0110 У  " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "005 " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "002 " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "031 с замком  " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "002 с замком " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "008 без замка " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "004" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "007 " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "0766 " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000017, NameRus = "764 " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "8017 фильцевая" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "511 (25-30) м" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "512 (22-27) ж.на низ" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "каб, 513 ж. На сред." });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "каб." });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "511-513" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "9015" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "шины-лапки 0716" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "0714" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "0717" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000018, NameRus = "943332" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "По Пирогову" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "23П-01 (d-94)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "23П-02 (d-102)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "23П-03 (d- 107)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "23П-04 (d- 116)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "23П-05 (d- 125)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "23П-06 (d- 135)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "23 П" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "16 Ф" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "16М б/замка" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "16Ф с замком" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "7ПЛ" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "стелька-металличская " });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "1140 шино-лапки" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "узел несущий" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000020, NameRus = "943333" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "8019 дерев-я" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "8019 дерев-я шинолапки" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "1205 АН" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "1205 БМ щикол.металлическая" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "8019 -дерев-я. б/шины" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "5260 (24-26)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "5260-01 (26,5-28,5)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "8019 (27-30)" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "8019-02" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "5260-23 П" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000019, NameRus = "5260" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000022, NameRus = "124 фигурн. Желобленное" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000022, NameRus = "121 пр." });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000022, NameRus = "124 фигурн." });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000022, NameRus = "122" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000022, NameRus = "121" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000023, NameRus = "120-3" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000026, NameRus = "811 голень" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000026, NameRus = "Металлический вертлуг 125" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000028, NameRus = "113" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000028, NameRus = "1197" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000028, NameRus = "119" });
                    context.ShifrPFabrikat.Add(new ShifrPFabrikat() { NamePFabrikatId = 1000028, NameRus = "Металлический вертлуг 125" });

                    context.SaveChanges();
                }

              
            }
            catch { }
        }
    }
    
}
