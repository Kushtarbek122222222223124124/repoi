﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Data.Enums
{
    public enum DocumentType
    {
        Passport = 1,
        Certificate = 2
    }
}
