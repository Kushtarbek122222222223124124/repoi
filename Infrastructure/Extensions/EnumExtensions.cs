﻿using rupoi.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;

namespace rupoi.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum e)
        {
            //return e.GetType().Name + "_" + e;

            var rm = new ResourceManager(typeof(LocResource));
            var resourceDisplayName = rm.GetString(e.GetType().Name + "_" + e);
            return string.IsNullOrWhiteSpace(resourceDisplayName) ? /*string.Format("[[{0}]]", e)*/ "" : resourceDisplayName;
        }
    }
}
