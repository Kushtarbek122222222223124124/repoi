﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rupoi.Framework;
using rupoi.Models.Dictionaries;
using rupoi.Resources;
using rupoi.Services.Interfaces;

namespace rupoi.Controllers
{
    public class DictionariesController : PhController
    {
        readonly IDictionariesService dicService = null;

        public DictionariesController(IDictionariesService dicService)
        {
            this.dicService = dicService;
        }


        [Route("/Dictionaries/Index.vue")]
        public IActionResult Index()
        {
            return PartialView();
        }

        public IActionResult List(string dictionaryType, DictionariesFileterModel filetr)
        {
            var pageModel = new DictionaryPageModel();
            pageModel.Title = GetDictionaryPageName(dictionaryType);

            pageModel.List = dicService.GetList(filetr, dictionaryType);

            return JsonData(pageModel);
        }

        [Route("/Dictionaries/Create.vue")]
        public IActionResult CreateView()
        {
            var model = new DictionaryModel();


            return PartialView(model);
        }

        public IActionResult Save(DictionaryModel model, string type)
        {
            dicService.Save(model, type);

            return JsonData(model);
        }


        private string GetDictionaryPageName(string dictionaryType)
        {
            var ret = "";
            if(dictionaryType == DictionaryTypesEnum.DiagnosisType.ToString())
            {
                ret = LocResource.DiagnosisType;
            }
            if (dictionaryType == DictionaryTypesEnum.ProductType.ToString())
            {
                ret = LocResource.ProductType;
            }

            return ret;
        }
    }
}
