﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using rupoi.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace rupoi.Utils
{
    /*public static class CustomHiddenHelperModelBinding
    {
        //This overload accepts single expression as parameter.
        public static MvcHtmlString Custom_HiddenFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            return Custom_HiddenFor(helper, expression, null);
        }

        //This overload accepts expression and htmlAttributes object as parameter.
        public static MvcHtmlString Custom_HiddenFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            //Fetching the metadata related to expression. This includes name of the property, model value of the property as well.
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            //Fetching the property name.
            string propertyName = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();

            //Creating a textarea tag using TagBuilder class.
            TagBuilder hidden = new TagBuilder("input");

            //Setting the type attribute to hidden to render hidden input field.
            hidden.Attributes.Add("type", "hidden");

            //Setting the name and id attribute.
            hidden.Attributes.Add("name", propertyName);
            hidden.Attributes.Add("id", propertyName);

            //Setting the value attribute of textbox with model value if present.
            if (metadata.Model != null)
            {
                hidden.Attributes.Add("value", metadata.Model.ToString());
            }
            //merging any htmlAttributes passed.
            hidden.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            //SelectExtensions.DropDownListFor<TModel, TValue>(helper, expression, );
            return MvcHtmlString.Create(hidden.ToString(TagRenderMode.Normal));
        }
    }*/

    public static class WidgetHelper
    {
        //This overload accepts expression and htmlAttributes object as parameter.


        public static HtmlString AdsLabelFor<TModel, TValue>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            if (helper == null) throw new ArgumentNullException(nameof(helper));
            if (expression == null) throw new ArgumentNullException(nameof(expression));

            ModelExpressionProvider modelExpressionProvider = (ModelExpressionProvider)helper.ViewContext.HttpContext.RequestServices.GetService(typeof(IModelExpressionProvider));

            var modelExplorer = modelExpressionProvider.CreateModelExpression(helper.ViewData, expression);



            var metadata = modelExplorer.Metadata;
            string htmlFieldName = metadata.DisplayName;
            string propertyName = metadata.PropertyName;

            string cls = "control-label";

            var helpText = "";
            var requiredText = "";

            var property = metadata.ContainerType.GetProperty(metadata.PropertyName);

            if (property != null)
            {
                var boolenInd = property.PropertyType.FullName != null && property.PropertyType.FullName.Contains("System.Boolean");

                AdsFieldHelpAttribute help =
                    (AdsFieldHelpAttribute)property.GetCustomAttribute(typeof(AdsFieldHelpAttribute));

                if (help != null)
                {
                    helpText = help.Help;
                    //helpText = "<span class=\"ads-help pull-right\" popover-append-to-body=\"\" data-titleclass=\"bordered-purple\" data-class=\"dark\" popover-placement=\"right\" popover=\"" + helpText + "\" data-original-title=\"\" title=\"\">&nbsp;</span>";
                    helpText = "<span class=\"" + (boolenInd ? "ads-help-ml10" : "ads-help") + " pull-right\" title=\"" + helpText + "\">&nbsp;</span>";
                    cls += " with-ads-help";
                }

                AdsRequiredFieldAttribute required =
                    (AdsRequiredFieldAttribute)property.GetCustomAttribute(typeof(AdsRequiredFieldAttribute));

                if (required != null && required.Required)
                {
                    cls += " required";
                    requiredText = "<span id=\"labelReqSpan_" + propertyName + "\" class=\"ads-required\">*</span>";
                }
            }

            return new HtmlString("<label class=\"" + cls + "\" for=\"" + propertyName + "\">" + htmlFieldName + requiredText + helpText + "</label>");
        }
    }
}